# Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

### [1.0.1](https://gitlab.com/bednic/rich-expression-builder/compare/1.0.0...1.0.1) (2022-05-23)


### Changed

* SQLResolver::params changed to protected ([9d87bbb](https://gitlab.com/bednic/rich-expression-builder/commit/9d87bbbe8f90bacf5f29f6431eb6eec7495067f4))
* value getter and setter changed to index ([9e9aa85](https://gitlab.com/bednic/rich-expression-builder/commit/9e9aa85b1cd3163e8bbef8088ba839d8bd71b5bc))

## [1.0.0](https://gitlab.com/bednic/rich-expression-builder/compare/0.1.3...1.0.0) (2022-03-30)


### ⚠ BREAKING CHANGES

* Remove HAS operator, Remove TArray.php

### Changed

* Operator enum, Field.php ([e28a02f](https://gitlab.com/bednic/rich-expression-builder/commit/e28a02fb4ae9d8720f1fefbd0be95fc9b8bf4a63))
* remove DQLResolver ([7047531](https://gitlab.com/bednic/rich-expression-builder/commit/70475318b722d847b2be8593c4a1dec18e7b124a))
* Remove TArray, narrow types in Ex methods, add args to Accessor.php, Remove HAS operator ([d871946](https://gitlab.com/bednic/rich-expression-builder/commit/d8719464d01ccc6b272971b3e2def8875a3db284))


### Added

* Add Doctrine resolvers ([c058ef5](https://gitlab.com/bednic/rich-expression-builder/commit/c058ef5540612c5ae21318eac47ce1bc4f124384))

### [0.1.3](https://gitlab.com/bednic/rich-expression-builder/compare/0.1.2...0.1.3) (2021-02-24)


### Added

* PostgreSQL dispatcher ([2649225](https://gitlab.com/bednic/rich-expression-builder/commit/26492259da086e216dc7efc51df7d5610fd48312))


### Fixed

* Accessor throws error when field does not exist ([260f7c1](https://gitlab.com/bednic/rich-expression-builder/commit/260f7c199f995f50c8afccd467118c4cc7b8d576)), closes [#1](https://gitlab.com/bednic/rich-expression-builder/issues/1)

### [0.1.2](https://gitlab.com/bednic/rich-expression-builder/compare/0.1.1...0.1.2) (2021-01-18)

### [0.1.1](https://gitlab.com/bednic/rich-expression-builder/compare/0.1.0...0.1.1) (2021-01-18)


### Changed

* array literal resolving ([f037170](https://gitlab.com/bednic/rich-expression-builder/commit/f037170da31f179b4cae064a25d239705be3fbd6))


### Fixed

* fix literal array arg ([34cae82](https://gitlab.com/bednic/rich-expression-builder/commit/34cae826da4ec51f992933e0544798d214e5b58a))
* indexOf return TNumeric ([28f675e](https://gitlab.com/bednic/rich-expression-builder/commit/28f675e01d7f21be8e9535a2ee659fe030598952))

## 0.1.0 (2020-11-23)


### Changed

* Restructuralize project from POC ([8dbbcbc](https://gitlab.com/bednic/rich-expression-builder/commit/8dbbcbc07e5542a1c8519a7f531506157d7bd6d4))


### Fixed

* replace generic expcetion ([110a784](https://gitlab.com/bednic/rich-expression-builder/commit/110a784d36075e5418df786c64466c0858372235))


### Added

* **Accessor:** Add Accessors ([a080ab6](https://gitlab.com/bednic/rich-expression-builder/commit/a080ab6fb23b24965a114635a04630b36a7d13b9))
* WIP SQLDispatcher ([6739a5d](https://gitlab.com/bednic/rich-expression-builder/commit/6739a5d8b88b1ebaf0b8e773eec8ddeab6a55692))

## 0.1.0 (2020-11-23)


### Changed

* Restructuralize project from POC ([8dbbcbc](https://gitlab.com/bednic/rich-expression-builder/commit/8dbbcbc07e5542a1c8519a7f531506157d7bd6d4))


### Fixed

* replace generic expcetion ([110a784](https://gitlab.com/bednic/rich-expression-builder/commit/110a784d36075e5418df786c64466c0858372235))


### Added

* **Accessor:** Add Accessors ([a080ab6](https://gitlab.com/bednic/rich-expression-builder/commit/a080ab6fb23b24965a114635a04630b36a7d13b9))
* WIP SQLDispatcher ([6739a5d](https://gitlab.com/bednic/rich-expression-builder/commit/6739a5d8b88b1ebaf0b8e773eec8ddeab6a55692))
