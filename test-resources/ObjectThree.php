<?php

declare(strict_types=1);

class ObjectThree
{
    public int $intProp = 1234;

    public string $strProp = 'string';

    public bool $boolProp = true;

    public float $floatProp = 12.34;

    public $nullProp = null;

    public array $arrProp = [1, 2, 3, 4];
}
