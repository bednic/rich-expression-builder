<?php

declare(strict_types=1);

namespace ExpressionBuilder\Test;

use ExpressionBuilder\Ex;
use ExpressionBuilder\Expression;
use ExpressionBuilder\Expression\Comparison;
use ExpressionBuilder\Expression\Field;
use ExpressionBuilder\Expression\Literal;
use ExpressionBuilder\Expression\Math;
use ExpressionBuilder\Expression\Method;
use ExpressionBuilder\Expression\Type\TBoolean;
use ExpressionBuilder\Expression\Type\TDateTime;
use ExpressionBuilder\Expression\Type\TNumeric;
use ExpressionBuilder\Expression\Type\TString;
use PHPUnit\Framework\TestCase;

class ExTest extends TestCase
{
    public function testStartsWith()
    {
        $ex = Ex::startsWith(new Literal(''), new Literal(''));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TBoolean::class, $ex);
    }

    public function testEq()
    {
        $ex = Ex::eq(new Literal(''), new Literal(''));
        $this->assertInstanceOf(Comparison::class, $ex);
        $this->assertInstanceOf(TBoolean::class, $ex);
    }

    public function testNe()
    {
        $ex = Ex::ne(new Literal(''), new Literal(''));
        $this->assertInstanceOf(Comparison::class, $ex);
        $this->assertInstanceOf(TBoolean::class, $ex);
    }

    public function testMul()
    {
        $ex = Ex::mul(new Literal(''), new Literal(''));
        $this->assertInstanceOf(Math::class, $ex);
        $this->assertInstanceOf(TNumeric::class, $ex);
    }

    public function testOr()
    {
        $ex = Ex::or(new Literal(''), new Literal(''));
        $this->assertInstanceOf(Comparison::class, $ex);
        $this->assertInstanceOf(TBoolean::class, $ex);
    }

    public function testTrim()
    {
        $ex = Ex::trim(new Literal(''));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TString::class, $ex);
    }

    public function testNot()
    {
        $ex = Ex::not(new Literal(''));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TBoolean::class, $ex);
    }

    public function testConcat()
    {
        $ex = Ex::concat(new Literal(''), new Literal(''));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TString::class, $ex);
    }

    public function testIndexOf()
    {
        $ex = Ex::indexOf(new Literal(''), new Literal(''));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TNumeric::class, $ex);
    }

    public function testDate()
    {
        $ex = Ex::date(new Literal(''));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TNumeric::class, $ex);
    }

    public function testSecond()
    {
        $ex = Ex::second(new Literal(''));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TNumeric::class, $ex);
    }

    public function testMod()
    {
        $ex = Ex::mod(new Literal(''), new Literal(''));
        $this->assertInstanceOf(Math::class, $ex);
        $this->assertInstanceOf(TNumeric::class, $ex);
    }

    public function testSub()
    {
        $ex = Ex::sub(new Literal(''), new Literal(''));
        $this->assertInstanceOf(Math::class, $ex);
        $this->assertInstanceOf(TNumeric::class, $ex);
    }

    public function testDay()
    {
        $ex = Ex::day(new Literal(''));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TNumeric::class, $ex);
    }

    public function testLiteral()
    {
        $ex = Ex::literal('');
        $this->assertInstanceOf(Literal::class, $ex);
        $this->assertInstanceOf(TNumeric::class, $ex);
        $this->assertInstanceOf(TString::class, $ex);
        $this->assertInstanceOf(TBoolean::class, $ex);
        $this->assertInstanceOf(TDateTime::class, $ex);
    }

    public function testLt()
    {
        $ex = Ex::day(new Literal(''));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TNumeric::class, $ex);
    }

    public function testContains()
    {
        $ex = Ex::day(new Literal(''));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TNumeric::class, $ex);
    }

    public function testEndsWith()
    {
        $ex = Ex::day(new Literal(''));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TNumeric::class, $ex);
    }

    public function testAdd()
    {
        $ex = Ex::day(new Literal(''));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TNumeric::class, $ex);
    }

    public function testMinute()
    {
        $ex = Ex::day(new Literal(''));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TNumeric::class, $ex);
    }

    public function testHas()
    {
        $ex = Ex::day(new Literal(''));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TNumeric::class, $ex);
    }

    public function testTime()
    {
        $ex = Ex::day(new Literal(''));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TNumeric::class, $ex);
    }

    public function testDiv()
    {
        $ex = Ex::day(new Literal(''));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TNumeric::class, $ex);
    }

    public function testField()
    {
        $ex = Ex::field('');
        $this->assertInstanceOf(Field::class, $ex);
        $this->assertInstanceOf(Expression::class, $ex);
    }

    public function testSubstring()
    {
        $ex = Ex::substring(new Literal(''), new Literal(''));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TString::class, $ex);
    }

    public function testGe()
    {
        $ex = Ex::ge(new Literal(''), new Literal(''));
        $this->assertInstanceOf(Comparison::class, $ex);
        $this->assertInstanceOf(TBoolean::class, $ex);
    }

    public function testMatchesPattern()
    {
        $ex = Ex::matchesPattern(new Literal(''), new Literal(''));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TBoolean::class, $ex);
    }

    public function testHour()
    {
        $ex = Ex::hour(new Literal(''));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TNumeric::class, $ex);
    }

    public function testFloor()
    {
        $ex = Ex::floor(new Literal(''));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TNumeric::class, $ex);
    }

    public function testLength()
    {
        $ex = Ex::length(new Literal(''));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TNumeric::class, $ex);
    }

    public function testMonth()
    {
        $ex = Ex::month(new Literal(''));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TNumeric::class, $ex);
    }

    public function testCeiling()
    {
        $ex = Ex::ceiling(new Literal(''));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TNumeric::class, $ex);
    }

    public function testToLower()
    {
        $ex = Ex::toLower(new Literal(''));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TString::class, $ex);
    }

    public function testAnd()
    {
        $ex = Ex::and(new Literal(''), new Literal(''));
        $this->assertInstanceOf(Comparison::class, $ex);
        $this->assertInstanceOf(TBoolean::class, $ex);
    }

    public function testRound()
    {
        $ex = Ex::round(new Literal(''));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TNumeric::class, $ex);
    }

    public function testLe()
    {
        $ex = Ex::le(new Literal(''), new Literal(''));
        $this->assertInstanceOf(Comparison::class, $ex);
        $this->assertInstanceOf(TBoolean::class, $ex);
    }

    public function testGt()
    {
        $ex = Ex::gt(new Literal(''), new Literal(''));
        $this->assertInstanceOf(Comparison::class, $ex);
        $this->assertInstanceOf(TBoolean::class, $ex);
    }

    public function testYear()
    {
        $ex = Ex::year(new Literal(''));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TNumeric::class, $ex);
    }

    public function testIn()
    {
        $ex = Ex::in(new Field(''), []);
        $this->assertInstanceOf(Comparison::class, $ex);
        $this->assertInstanceOf(TBoolean::class, $ex);
    }

    public function testToUpper()
    {
        $ex = Ex::toUpper(new Literal(''));
        $this->assertInstanceOf(Method::class, $ex);
        $this->assertInstanceOf(TString::class, $ex);
    }
}
