<?php

/**
 * Created by tomas
 * at 15.02.2021 19:53
 */

declare(strict_types=1);

namespace ExpressionBuilder\Test\Dispatcher;

use ExpressionBuilder\Dispatcher\PostgresSQLResolver;
use ExpressionBuilder\Ex;
use ExpressionBuilder\Expression\Literal;
use PDO;
use PHPUnit\Framework\TestCase;

class PostgresSQLResolverTest extends TestCase
{
    private static PDO $pdo;

    public static function setUpBeforeClass(): void
    {
        $host      = getenv('POSTGRES_HOST') ? getenv('POSTGRES_HOST') : 'localhost';
        $user      = getenv('POSTGRES_USER') ? getenv('POSTGRES_USER') : 'user';
        $password  = getenv('POSTGRES_PASSWORD') ? getenv('POSTGRES_PASSWORD') : 'secret';
        self::$pdo = new PDO("pgsql:host=$host;port=5432;user=$user;password=$password");
        self::$pdo->exec(
            'CREATE TABLE IF NOT EXISTS test (key varchar, str varchar, int integer, flt double precision);'
        );
        self::$pdo->exec('TRUNCATE test;');
        self::$pdo->exec("INSERT INTO test VALUES ('key', 'foo',1,1.1);");
    }


    public function testMethods()
    {
        $dispatcher = new PostgresSQLResolver();
        $ex         = Ex::contains(Ex::field('str'), Ex::literal('fo'));
        $filter     = $dispatcher->dispatch($ex);
        $stm        = self::$pdo->prepare("SELECT * FROM test WHERE $filter;");
        $stm->execute($dispatcher->getParams());
        $result = $stm->fetchAll();
        $this->assertNotEmpty($result);

        $dispatcher = new PostgresSQLResolver();
        $ex         = Ex::eq(
            Ex::substring(Ex::field('str'), Ex::literal(3)),
            Ex::literal('o')
        );
        $filter     = $dispatcher->dispatch($ex);
        $stm        = self::$pdo->prepare("SELECT * fROM test WHERE $filter;");
        $stm->execute($dispatcher->getParams());
        $result = $stm->fetchAll();
        $this->assertNotEmpty($result);
    }

    public function testMath()
    {
        $ex         = Ex::eq(
            Ex::literal(1),
            Ex::div(
                Ex::literal(1),
                Ex::mul(
                    Ex::literal(1),
                    Ex::mod(
                        Ex::literal(3),
                        Ex::sub(
                            Ex::literal(4),
                            Ex::add(
                                Ex::literal(1),
                                Ex::literal(1)
                            )
                        )
                    )
                )
            )
        );
        $dispatcher = new PostgresSQLResolver();
        $filter     = $dispatcher->dispatch($ex);
        $stm        = self::$pdo->prepare("SELECT $filter");
        $stm->execute($dispatcher->getParams());
        $result = $stm->fetchColumn();
        $this->assertTrue($result);
    }

    public function testField()
    {
        $ex         = Ex::or(
            Ex::eq(Ex::field('key'), Ex::literal('key')),
            Ex::ne(Ex::literal(null), Ex::field('key'))
        );
        $dispatcher = new PostgresSQLResolver();
        $filter     = $dispatcher->dispatch($ex);
        $stm        = self::$pdo->prepare("SELECT * from test t WHERE $filter;");
        $stm->execute($dispatcher->getParams());
        $result = $stm->fetchAll();
        $this->assertNotFalse($result);
    }

    public function testBetween()
    {
        $ex         = Ex::be(Ex::field('flt'), Ex::literal(1), Ex::literal(2));
        $dispatcher = new PostgresSQLResolver();
        $filter     = $dispatcher->dispatch($ex);
        $stm        = self::$pdo->prepare("SELECT * from test t WHERE $filter;");
        $stm->execute($dispatcher->getParams());
        $result = $stm->fetchAll();
        $this->assertNotFalse($result);
    }

    public function testIn()
    {
        $ex         = Ex::in(Ex::field('flt'), [1, 2]);
        $dispatcher = new PostgresSQLResolver();
        $filter     = $dispatcher->dispatch($ex);
        $stm = self::$pdo->prepare("SELECT * from test t WHERE $filter;");
        $stm->execute($dispatcher->getParams());
        $result = $stm->fetchAll();
        $this->assertNotFalse($result);
    }
}
