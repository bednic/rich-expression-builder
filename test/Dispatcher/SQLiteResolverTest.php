<?php

declare(strict_types=1);

namespace ExpressionBuilder\Test\Dispatcher;

use DateTime;
use ExpressionBuilder\Dispatcher;
use ExpressionBuilder\Dispatcher\SQLiteResolver;
use ExpressionBuilder\Ex;
use ExpressionBuilder\Exception\UnknownArithmeticOperation;
use ExpressionBuilder\Exception\UnknownComparisonOperator;
use ExpressionBuilder\Exception\UnknownExpression;
use ExpressionBuilder\Exception\UnknownFunction;
use ExpressionBuilder\Expression;
use ExpressionBuilder\Expression\Comparison;
use ExpressionBuilder\Expression\Field;
use ExpressionBuilder\Expression\Literal;
use ExpressionBuilder\Expression\Math;
use ExpressionBuilder\Expression\Method;
use PDO;
use PHPUnit\Framework\TestCase;

class SQLiteResolverTest extends TestCase
{
    private static PDO $pdo;

    public static function setUpBeforeClass(): void
    {
        self::$pdo = new PDO('sqlite::memory:');
        self::$pdo->exec(
            '
CREATE TABLE store (
string TEXT,
number REAL,
integer INTEGER,
bool BLOB,
date BLOB,
nill NULL
)'
        );
        $stm  = self::$pdo->prepare('INSERT INTO store VALUES (?,?,?,?,?,?)');
        $data = [
            'lorem ipsum',
            12.34,
            1234,
            true,
            (new DateTime('2020-01-01 12:30:45'))->format(DATE_ATOM),
            null
        ];
        $stm->execute($data);
    }

    public function testUnknownExpression()
    {
        $this->expectException(UnknownExpression::class);
        $dispatcher = new SQLiteResolver();
        $expression = new class implements Expression {
            /**
             * @param Dispatcher $dispatcher
             *
             * @return mixed
             */
            public function resolve(Dispatcher $dispatcher): mixed
            {
                return $dispatcher->dispatch($this);
            }
        };
        $expression->resolve($dispatcher);
    }

    public function testComparison()
    {
        $dispatcher = new SQLiteResolver();
        $expression = Ex::eq(Ex::field('string'), Ex::literal('lorem ipsum'));
        $filter     = $expression->resolve($dispatcher);
        $sql        = 'SELECT * FROM store WHERE ' . $filter;
        $stm        = self::$pdo->prepare($sql);
        $stm->execute($dispatcher->getParams());
        $result = $stm->fetchAll(PDO::FETCH_ASSOC);
        $this->assertCount(1, $result);

        $dispatcher = new SQLiteResolver();
        $expression = Ex::ne(Ex::field('string'), Ex::literal('string'));
        $filter     = $expression->resolve($dispatcher);
        $sql        = 'SELECT * FROM store WHERE ' . $filter;
        $stm        = self::$pdo->prepare($sql);
        $stm->execute($dispatcher->getParams());
        $result = $stm->fetchAll(PDO::FETCH_ASSOC);
        $this->assertCount(1, $result);

        $dispatcher = new SQLiteResolver();
        $expression = Ex::lt(Ex::field('integer'), Ex::literal(1300));
        $filter     = $expression->resolve($dispatcher);
        $sql        = 'SELECT * FROM store WHERE ' . $filter;
        $stm        = self::$pdo->prepare($sql);
        $stm->execute($dispatcher->getParams());
        $result = $stm->fetchAll(PDO::FETCH_ASSOC);
        $this->assertCount(1, $result);

        $dispatcher = new SQLiteResolver();
        $expression = Ex::le(Ex::field('integer'), Ex::literal(1234));
        $filter     = $expression->resolve($dispatcher);
        $sql        = 'SELECT * FROM store WHERE ' . $filter;
        $stm        = self::$pdo->prepare($sql);
        $stm->execute($dispatcher->getParams());
        $result = $stm->fetchAll(PDO::FETCH_ASSOC);
        $this->assertCount(1, $result);

        $dispatcher = new SQLiteResolver();
        $expression = Ex::gt(Ex::field('number'), Ex::literal(12.1));
        $filter     = $expression->resolve($dispatcher);
        $sql        = 'SELECT * FROM store WHERE ' . $filter;
        $stm        = self::$pdo->prepare($sql);
        $stm->execute($dispatcher->getParams());
        $result = $stm->fetchAll(PDO::FETCH_ASSOC);
        $this->assertCount(1, $result);

        $dispatcher = new SQLiteResolver();
        $expression = Ex::ge(Ex::field('number'), Ex::literal(12.34));
        $filter     = $expression->resolve($dispatcher);
        $sql        = 'SELECT * FROM store WHERE ' . $filter;
        $stm        = self::$pdo->prepare($sql);
        $stm->execute($dispatcher->getParams());
        $result = $stm->fetchAll(PDO::FETCH_ASSOC);
        $this->assertCount(1, $result);

        $dispatcher = new SQLiteResolver();
        $expression = Ex::ge(Ex::field('number'), Ex::literal(12.34));
        $filter     = $expression->resolve($dispatcher);
        $sql        = 'SELECT * FROM store WHERE ' . $filter;
        $stm        = self::$pdo->prepare($sql);
        $stm->execute($dispatcher->getParams());
        $result = $stm->fetchAll(PDO::FETCH_ASSOC);
        $this->assertCount(1, $result);
        try {
            $dispatcher = new SQLiteResolver();
            $expression = Ex::in(Ex::field('number'), [12.34]);
            $filter     = $expression->resolve($dispatcher);
            $sql        = 'SELECT * FROM store WHERE ' . $filter;
            $stm        = self::$pdo->prepare($sql);
            $stm->execute($dispatcher->getParams());
            $result = $stm->fetchAll(PDO::FETCH_ASSOC);
            $this->assertCount(1, $result);
        } catch (UnknownComparisonOperator $exception) {
            $this->assertInstanceOf(UnknownComparisonOperator::class, $exception);
        }

        $dispatcher = new SQLiteResolver();
        $expression = Ex::and(
            Ex::eq(Ex::field('integer'), Ex::literal(1234)),
            Ex::eq(Ex::field('number'), Ex::literal(12.34))
        );
        $filter     = $expression->resolve($dispatcher);
        $sql        = 'SELECT * FROM store WHERE ' . $filter;
        $stm        = self::$pdo->prepare($sql);
        $stm->execute($dispatcher->getParams());
        $result = $stm->fetchAll(PDO::FETCH_ASSOC);
        $this->assertCount(1, $result);

        $dispatcher = new SQLiteResolver();
        $expression = Ex::or(
            Ex::eq(Ex::field('integer'), Ex::literal(1234)),
            Ex::eq(Ex::field('number'), Ex::literal(12.34))
        );
        $filter     = $expression->resolve($dispatcher);
        $sql        = 'SELECT * FROM store WHERE ' . $filter;
        $stm        = self::$pdo->prepare($sql);
        $stm->execute($dispatcher->getParams());
        $result = $stm->fetchAll(PDO::FETCH_ASSOC);
        $this->assertCount(1, $result);

        $dispatcher = new SQLiteResolver();
        $expression = Ex::in(Ex::field('integer'), [1234, 2345, 3456, 4567]);
        $filter     = $expression->resolve($dispatcher);
        $sql        = 'SELECT * FROM store WHERE ' . $filter;
        $stm        = self::$pdo->prepare($sql);
        $stm->execute($dispatcher->getParams());
        $result = $stm->fetchAll(PDO::FETCH_ASSOC);
        $this->assertCount(1, $result);
    }

    public function testComparisonOperatorIn()
    {
        $dispatcher = new SQLiteResolver();
        $expression = Ex::in(Ex::field('integer'), [1234, 2345, 3456, 4567]);
        $filter     = $expression->resolve($dispatcher);
        $sql        = 'SELECT * FROM store WHERE ' . $filter;
        $stm        = self::$pdo->prepare($sql);
        $stm->execute($dispatcher->getParams());
        $result = $stm->fetchAll(PDO::FETCH_ASSOC);
        $this->assertCount(1, $result);
    }

    public function testArithmetic()
    {
        $dispatcher = new SQLiteResolver();
        $expression = Ex::add(Ex::literal(8), Ex::literal(2));
        $filter     = $expression->resolve($dispatcher);
        $sql        = 'SELECT ' . $filter;
        $stm        = self::$pdo->prepare($sql);
        $stm->execute($dispatcher->getParams());
        $result = $stm->fetchColumn(0);
        $this->assertEquals(10, $result);

        $dispatcher = new SQLiteResolver();
        $expression = Ex::sub(Ex::literal(8), Ex::literal(2));
        $filter     = $expression->resolve($dispatcher);
        $sql        = 'SELECT ' . $filter;
        $stm        = self::$pdo->prepare($sql);
        $stm->execute($dispatcher->getParams());
        $result = $stm->fetchColumn(0);
        $this->assertEquals(6, $result);

        $dispatcher = new SQLiteResolver();
        $expression = Ex::mul(Ex::literal(8), Ex::literal(2));
        $filter     = $expression->resolve($dispatcher);
        $sql        = 'SELECT ' . $filter;
        $stm        = self::$pdo->prepare($sql);
        $stm->execute($dispatcher->getParams());
        $result = $stm->fetchColumn(0);
        $this->assertEquals(16, $result);

        $dispatcher = new SQLiteResolver();
        $expression = Ex::div(Ex::literal(8), Ex::literal(2));
        $filter     = $expression->resolve($dispatcher);
        $sql        = 'SELECT ' . $filter;
        $stm        = self::$pdo->prepare($sql);
        $stm->execute($dispatcher->getParams());
        $result = $stm->fetchColumn(0);
        $this->assertEquals(4, $result);

        $dispatcher = new SQLiteResolver();
        $expression = Ex::mod(Ex::literal(8), Ex::literal(2));
        $filter     = $expression->resolve($dispatcher);
        $sql        = 'SELECT ' . $filter;
        $stm        = self::$pdo->prepare($sql);
        $stm->execute($dispatcher->getParams());
        $result = $stm->fetchColumn(0);
        $this->assertEquals(0, $result);
    }

    public function testFunction()
    {
        $dispatcher = new SQLiteResolver();
        $expression = Ex::length(Ex::literal('test'));
        $filter     = $expression->resolve($dispatcher);
        $sql        = 'SELECT ' . $filter;
        $stm        = self::$pdo->prepare($sql);
        $stm->execute($dispatcher->getParams());
        $result = $stm->fetchColumn(0);
        $this->assertEquals(4, $result);

        $dispatcher = new SQLiteResolver();
        $expression = Ex::contains(Ex::literal('test'), Ex::literal('s'));
        $filter     = $expression->resolve($dispatcher);
        $sql        = 'SELECT ' . $filter;
        $stm        = self::$pdo->prepare($sql);
        $stm->execute($dispatcher->getParams());
        $result = $stm->fetchColumn(0);
        $this->assertEquals(true, $result);

        //        $expression = Ex::eq(Ex::concat(Ex::literal('test'), Ex::literal('s')), Ex::literal('tests'));
        //        $filter     = $expression->resolve(new SQLiteResolver());
        //        $result     = array_filter($arr, $filter);
        //        $this->assertCount(2, $result);
        //
        //        $expression = Ex::endsWith(Ex::literal('test'), Ex::literal('t'));
        //        $filter     = $expression->resolve(new SQLiteResolver());
        //        $result     = array_filter($arr, $filter);
        //        $this->assertCount(2, $result);
        //
        //        $expression = Ex::startsWith(Ex::literal('test'), Ex::literal('t'));
        //        $filter     = $expression->resolve(new SQLiteResolver());
        //        $result     = array_filter($arr, $filter);
        //        $this->assertCount(2, $result);
        //
        //        $expression = Ex::not(Ex::startsWith(Ex::literal('test'), Ex::literal('s')));
        //        $filter     = $expression->resolve(new SQLiteResolver());
        //        $result     = array_filter($arr, $filter);
        //        $this->assertCount(2, $result);
        //
        //        $expression = Ex::eq(Ex::trim(Ex::literal('    test   ')), Ex::literal('test'));
        //        $filter     = $expression->resolve(new SQLiteResolver());
        //        $result     = array_filter($arr, $filter);
        //        $this->assertCount(2, $result);
        //
        //        $expression = Ex::eq(Ex::indexOf(Ex::literal('test'), Ex::literal('es')), Ex::literal(1));
        //        $filter     = $expression->resolve(new SQLiteResolver());
        //        $result     = array_filter($arr, $filter);
        //        $this->assertCount(2, $result);
        //
        //        $expression = Ex::eq(Ex::substring(Ex::literal('test'), Ex::literal(1)), Ex::literal('est'));
        //        $filter     = $expression->resolve(new SQLiteResolver());
        //        $result     = array_filter($arr, $filter);
        //        $this->assertCount(2, $result);
        //
        //        $expression = Ex::matchesPattern(Ex::literal('test'), Ex::literal('/^t.*t$/'));
        //        $filter     = $expression->resolve(new SQLiteResolver());
        //        $result     = array_filter($arr, $filter);
        //        $this->assertCount(2, $result);
        //
        //        $expression = Ex::eq(Ex::toLower(Ex::literal('TEST')), Ex::literal('test'));
        //        $filter     = $expression->resolve(new SQLiteResolver());
        //        $result     = array_filter($arr, $filter);
        //        $this->assertCount(2, $result);
        //
        //        $expression = Ex::eq(Ex::toUpper(Ex::literal('test')), Ex::literal('TEST'));
        //        $filter     = $expression->resolve(new SQLiteResolver());
        //        $result     = array_filter($arr, $filter);
        //        $this->assertCount(2, $result);
        //
        //        $expression = Ex::eq(Ex::toUpper(Ex::literal('test')), Ex::literal('TEST'));
        //        $filter     = $expression->resolve(new SQLiteResolver());
        //        $result     = array_filter($arr, $filter);
        //        $this->assertCount(2, $result);
        //
        //        $expression = Ex::eq(Ex::date(Ex::literal('2020-12-31 15:30:45')), Ex::literal('2020-12-31'));
        //        $filter     = $expression->resolve(new SQLiteResolver());
        //        $result     = array_filter($arr, $filter);
        //        $this->assertCount(2, $result);
        //
        //        $expression = Ex::eq(Ex::year(Ex::literal('2020-12-31 15:30:45')), Ex::literal('2020'));
        //        $filter     = $expression->resolve(new SQLiteResolver());
        //        $result     = array_filter($arr, $filter);
        //        $this->assertCount(2, $result);
        //
        //        $expression = Ex::eq(Ex::month(Ex::literal('2020-12-31 15:30:45')), Ex::literal('12'));
        //        $filter     = $expression->resolve(new SQLiteResolver());
        //        $result     = array_filter($arr, $filter);
        //        $this->assertCount(2, $result);
        //
        //        $expression = Ex::eq(Ex::day(Ex::literal('2020-12-31 15:30:45')), Ex::literal('31'));
        //        $filter     = $expression->resolve(new SQLiteResolver());
        //        $result     = array_filter($arr, $filter);
        //        $this->assertCount(2, $result);
        //
        //        $expression = Ex::eq(Ex::hour(Ex::literal('2020-12-31 15:30:45')), Ex::literal('15'));
        //        $filter     = $expression->resolve(new SQLiteResolver());
        //        $result     = array_filter($arr, $filter);
        //        $this->assertCount(2, $result);
        //
        //        $expression = Ex::eq(Ex::minute(Ex::literal('2020-12-31 15:30:45')), Ex::literal('30'));
        //        $filter     = $expression->resolve(new SQLiteResolver());
        //        $result     = array_filter($arr, $filter);
        //        $this->assertCount(2, $result);
        //
        //        $expression = Ex::eq(Ex::second(Ex::literal('2020-12-31 15:30:45')), Ex::literal('45'));
        //        $filter     = $expression->resolve(new SQLiteResolver());
        //        $result     = array_filter($arr, $filter);
        //        $this->assertCount(2, $result);
        //
        //        $expression = Ex::eq(Ex::time(Ex::literal('2020-12-31 15:30:45')), Ex::literal('15:30:45'));
        //        $filter     = $expression->resolve(new SQLiteResolver());
        //        $result     = array_filter($arr, $filter);
        //        $this->assertCount(2, $result);
        //
        //        $expression = Ex::eq(Ex::ceiling(Ex::literal(1.2)), Ex::literal(2));
        //        $filter     = $expression->resolve(new SQLiteResolver());
        //        $result     = array_filter($arr, $filter);
        //        $this->assertCount(2, $result);
        //
        //        $expression = Ex::eq(Ex::floor(Ex::literal(1.2)), Ex::literal(1));
        //        $filter     = $expression->resolve(new SQLiteResolver());
        //        $result     = array_filter($arr, $filter);
        //        $this->assertCount(2, $result);
        //
        //        $expression = Ex::eq(Ex::round(Ex::literal(1.2)), Ex::literal(1));
        //        $filter     = $expression->resolve(new SQLiteResolver());
        //        $result     = array_filter($arr, $filter);
        //        $this->assertCount(2, $result);
    }

    public function testBetween()
    {
        $ex         = Ex::be(Ex::field('number'), Ex::literal(12), Ex::literal(13));
        $dispatcher = new SQLiteResolver();
        $filter     = $ex->resolve($dispatcher);
        $stm        = self::$pdo->prepare("SELECT * from store t WHERE $filter;");
        $stm->execute($dispatcher->getParams());
        $result = $stm->fetchAll();
        $this->assertNotFalse($result);
    }
}
