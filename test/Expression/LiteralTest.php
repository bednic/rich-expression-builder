<?php

declare(strict_types=1);

namespace ExpressionBuilder\Test\Expression;

use ExpressionBuilder\Expression\Literal;
use PHPUnit\Framework\TestCase;

class LiteralTest extends TestCase
{
    public function testArrayValue()
    {
        $value1 = [1, 2, 3];
        $value2 = [new Literal(1), new Literal(2), new Literal(3)];
        $l1     = new Literal($value1);
        $this->assertEquals($value2, $l1->getValue());
        $l2 = new Literal($value2);
        $this->assertEquals($value2, $l2->getValue());
    }
}
