<?php

declare(strict_types=1);

namespace ExpressionBuilder\Test\Accessor;

use ExpressionBuilder\Accessor\ObjectPropertyAccessor;
use ExpressionBuilder\Exception\ExpressionBuilderError;
use ExpressionBuilder\Expression\Field;
use ObjectOne;
use PHPUnit\Framework\TestCase;
use TypeError;

/**
 * Class ObjectPropertyAccessorTest
 *
 * @package ExpressionBuilder\Test\Accessor
 */
class ObjectPropertyAccessorTest extends TestCase
{
    public function testFieldAccess()
    {
        $accessor = new ObjectPropertyAccessor();
        $object   = new ObjectOne();

        $this->assertEquals('private', $accessor(new Field('priProp'), [$object]));
        $this->assertEquals('protected', $accessor(new Field('proProp'), [$object]));
        $this->assertEquals('public', $accessor(new Field('pubProp'), [$object]));
    }

    public function testDeepAccess()
    {
        $field    = new Field('two.intProp');
        $accessor = new ObjectPropertyAccessor();
        $object   = new ObjectOne();

        $this->assertEquals(1234, $accessor($field, [$object]));

        $field    = new Field('two.three.intProp');
        $accessor = new ObjectPropertyAccessor();
        $object   = new ObjectOne();

        $this->assertEquals(1234, $accessor($field, [$object]));
    }

    public function testNonExistingProperty()
    {
        $this->expectException(TypeError::class);
        $field    = new Field('two.three.intProp.nonExist');
        $accessor = new ObjectPropertyAccessor();
        $object   = new ObjectOne();
        $accessor($field, [$object]);
    }
}
