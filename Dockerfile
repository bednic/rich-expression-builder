# RUNTIME
FROM php:8.1

RUN apt-get update -y
RUN apt-get install -y libpq-dev vim git zip unzip
RUN docker-php-ext-install pdo_pgsql pdo_mysql
RUN pecl install xdebug
RUN docker-php-ext-enable xdebug

WORKDIR /var/www

COPY ./php.ini /usr/local/etc/php/

# COMPOSER
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
