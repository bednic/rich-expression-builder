<?php

namespace ExpressionBuilder\Exception;

use RuntimeException;

/**
 * Class MissingDependency
 *
 * @package ExpressionBuilder\Exception
 */
class MissingDependency extends RuntimeException
{
}
