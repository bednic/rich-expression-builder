<?php

declare(strict_types=1);

namespace ExpressionBuilder\Exception;

/**
 * Class UnknownExpression
 *
 * @package ExpressionBuilder\Exception
 */
class UnknownExpression extends ExpressionBuilderError
{
    public function __construct($className)
    {
        parent::__construct(sprintf('Unknown expression %s', $className), 5001);
    }
}
