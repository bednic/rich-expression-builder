<?php

declare(strict_types=1);

namespace ExpressionBuilder\Exception;

/**
 * Class UnknownFunction
 *
 * @package ExpressionBuilder\Exception
 */
class UnknownFunction extends ExpressionBuilderError
{
    public function __construct($fn)
    {
        parent::__construct(sprintf('Unknown function %s', $fn), 5003);
    }
}
