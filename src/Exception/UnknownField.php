<?php

/**
 * Created by tomas
 * at 19.02.2021 19:56
 */

declare(strict_types=1);

namespace ExpressionBuilder\Exception;

/**
 * Class UnknownField
 *
 * @package ExpressionBuilder\Exception
 */
class UnknownField extends ExpressionBuilderError
{
    public function __construct($object, $field)
    {
        parent::__construct(sprintf('Unknown field %s of object %s', $field, get_class($object)), 5005);
    }
}
