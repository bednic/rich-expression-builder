<?php

/**
 * Created by uzivatel
 * at 24.03.2022 10:52
 */

declare(strict_types=1);

namespace ExpressionBuilder\Exception;

/**
 * Class InvalidArgument
 *
 * @package ExpressionBuilder\Exception
 */
class InvalidArgument extends ExpressionBuilderError
{
}
