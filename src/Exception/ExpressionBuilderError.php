<?php

declare(strict_types=1);

namespace ExpressionBuilder\Exception;

use Exception;

/**
 * Class ExpressionBuilderError
 *
 * @package ExpressionBuilder\Exception
 */
abstract class ExpressionBuilderError extends Exception
{
    protected $code = 5000;
}
