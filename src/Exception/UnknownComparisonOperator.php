<?php

declare(strict_types=1);

namespace ExpressionBuilder\Exception;

use ExpressionBuilder\Expression\Operator;

/**
 * Class UnknownOperator
 *
 * @package ExpressionBuilder\Exception
 */
class UnknownComparisonOperator extends ExpressionBuilderError
{
    public function __construct(Operator $operator)
    {
        parent::__construct(sprintf('Unknown operator %s', $operator->name), 5002);
    }
}
