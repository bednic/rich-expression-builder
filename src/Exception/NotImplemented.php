<?php

/**
 * Created by uzivatel
 * at 23.03.2022 12:38
 */

declare(strict_types=1);

namespace ExpressionBuilder\Exception;

/**
 * Class NotImplemented
 *
 * @package ExpressionBuilder\Exception
 */
class NotImplemented extends ExpressionBuilderError
{
    public function __construct($operator)
    {
        parent::__construct(sprintf('Operator %s is not implemented.', $operator), 5006);
    }
}
