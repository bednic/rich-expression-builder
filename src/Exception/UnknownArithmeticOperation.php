<?php

declare(strict_types=1);

namespace ExpressionBuilder\Exception;

/**
 * Class UnknownArithmeticOperation
 *
 * @package ExpressionBuilder\Exception
 */
class UnknownArithmeticOperation extends ExpressionBuilderError
{
    public function __construct($operator)
    {
        parent::__construct(sprintf('Unknown arithmetic operation %s', $operator), 5004);
    }
}
