<?php

declare(strict_types=1);

namespace ExpressionBuilder;

use ExpressionBuilder\Exception\ExpressionBuilderError;

/**
 * Interface Expression
 *
 * @package ExpressionBuilder
 */
interface Expression
{
    /**
     * @param Dispatcher $dispatcher
     *
     * @return mixed
     * @throws ExpressionBuilderError
     */
    public function resolve(Dispatcher $dispatcher): mixed;
}
