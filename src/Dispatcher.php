<?php

declare(strict_types=1);

namespace ExpressionBuilder;

use ExpressionBuilder\Exception\ExpressionBuilderError;

/**
 * Interface Dispatcher
 *
 * @package ExpressionBuilder
 */
interface Dispatcher
{
    /**
     * @param Expression $expression
     *
     * @return mixed
     * @throws ExpressionBuilderError
     */
    public function dispatch(Expression $expression);
}
