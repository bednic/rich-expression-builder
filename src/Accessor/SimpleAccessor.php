<?php

namespace ExpressionBuilder\Accessor;

use ExpressionBuilder\Accessor;
use ExpressionBuilder\Expression\Field;

/**
 * Class SimpleAccessor
 *
 * @package ExpressionBuilder\Accessor
 */
class SimpleAccessor implements Accessor
{
    /**
     * @inheritDoc
     */
    public function __invoke(Field $field, array $args = []): string
    {
        return $field->getName();
    }
}
