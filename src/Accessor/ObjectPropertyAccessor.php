<?php

declare(strict_types=1);

namespace ExpressionBuilder\Accessor;

use ExpressionBuilder\Accessor;
use ExpressionBuilder\Exception\InvalidArgument;
use ExpressionBuilder\Exception\UnknownField;
use ExpressionBuilder\Expression\Field;

/**
 * Class ObjectPropertyAccessor for ClosureDispatcher
 *
 * @package ExpressionBuilder
 */
class ObjectPropertyAccessor implements Accessor
{
    /**
     * @inheritDoc
     */
    public function __invoke(Field $field, array $args = []): mixed
    {
        if (empty($args) || !is_object($args[0])) {
            throw new InvalidArgument("Argument with accessed Object missing.");
        }
        $fields = explode('.', $field->getName());
        $value = $args[0];
        foreach ($fields as $field) {
            $value = $this->getValue($value, $field);
        }
        return $value;
    }

    /**
     * @param object $object
     * @param string $field
     *
     * @return mixed
     * @throws UnknownField
     */
    private function getValue(object $object, string $field): mixed
    {
        if (method_exists($object, $field)) {
            return $object->$field();
        }
        foreach (['get', 'is'] as $prefix) {
            $accessor = $prefix . ucfirst($field);
            if (method_exists($object, $accessor)) {
                return $object->{$accessor}();
            }
        }
        if (property_exists($object, $field)) {
            return $object->$field;
        }
        throw new UnknownField($object, $field);
    }
}
