<?php

declare(strict_types=1);

namespace ExpressionBuilder\Expression;

use ExpressionBuilder\Expression;

/**
 * Interface Binary
 *
 * @package ExpressionBuilder\Expression
 */
interface Binary extends Expression
{
    /**
     * @return Expression
     */
    public function getLeft(): Expression;

    /**
     * @return Expression
     */
    public function getRight(): Expression;

    /**
     * @return Operator
     */
    public function getOp(): Operator;
}
