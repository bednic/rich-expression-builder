<?php

declare(strict_types=1);

namespace ExpressionBuilder\Expression\Type;

use ExpressionBuilder\Expression;

/**
 * Interface TString
 *
 * @package ExpressionBuilder\Expression
 */
interface TString extends Expression
{
}
