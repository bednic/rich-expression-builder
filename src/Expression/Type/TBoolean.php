<?php

declare(strict_types=1);

namespace ExpressionBuilder\Expression\Type;

use ExpressionBuilder\Expression;

/**
 * Interface TBoolean
 *
 * @package ExpressionBuilder\Expression
 */
interface TBoolean extends Expression
{
}
