<?php

declare(strict_types=1);

namespace ExpressionBuilder\Expression\Type;

use ExpressionBuilder\Expression;

/**
 * Interface TNumeric
 *
 * @package ExpressionBuilder\Expression
 */
interface TNumeric extends Expression
{
}
