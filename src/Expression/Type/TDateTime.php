<?php

declare(strict_types=1);

namespace ExpressionBuilder\Expression\Type;

use ExpressionBuilder\Expression;

/**
 * Interface TDateTime
 *
 * @package ExpressionBuilder\Expression
 */
interface TDateTime extends Expression
{
}
