<?php

declare(strict_types=1);

namespace ExpressionBuilder\Expression;

use ExpressionBuilder\Dispatcher;
use ExpressionBuilder\Exception\ExpressionBuilderError;
use ExpressionBuilder\Expression\Type\TNumeric;

/**
 * Class Math
 *
 * @package ExpressionBuilder\Expression
 */
class Math implements Binary, TNumeric
{
    /**
     * @var TNumeric
     */
    private TNumeric $left;
    /**
     * @var TNumeric
     */
    private TNumeric $right;
    /**
     * @var Operator
     */
    private Operator $op;

    /**
     * Math constructor.
     *
     * @param TNumeric $left
     * @param Operator $operator
     * @param TNumeric $right
     */
    public function __construct(TNumeric $left, Operator $operator, TNumeric $right)
    {
        $this->left  = $left;
        $this->right = $right;
        $this->op    = $operator;
    }

    /**
     * @return TNumeric
     */
    public function getLeft(): TNumeric
    {
        return $this->left;
    }

    /**
     * @return TNumeric
     */
    public function getRight(): TNumeric
    {
        return $this->right;
    }

    /**
     * @return Operator
     */
    public function getOp(): Operator
    {
        return $this->op;
    }

    /**
     * @param Dispatcher $dispatcher
     *
     * @return float|int
     * @throws ExpressionBuilderError
     */
    public function resolve(Dispatcher $dispatcher): mixed
    {
        return $dispatcher->dispatch($this);
    }
}
