<?php

declare(strict_types=1);

namespace ExpressionBuilder\Expression;

use ExpressionBuilder\Expression;
use JetBrains\PhpStorm\ArrayShape;

/**
 * Interface Functional
 *
 * @package ExpressionBuilder\Expression
 */
interface Functional extends Expression
{
    /**
     * @return Operator
     */
    public function getFn(): Operator;


    /**
     * @return Expression[]
     */
    public function getArgs(): array;
}
