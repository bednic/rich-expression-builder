<?php

declare(strict_types=1);

namespace ExpressionBuilder\Expression;

use ExpressionBuilder\Dispatcher;
use ExpressionBuilder\Exception\ExpressionBuilderError;
use ExpressionBuilder\Expression\Type\TBoolean;
use ExpressionBuilder\Expression\Type\TDateTime;
use ExpressionBuilder\Expression\Type\TNumeric;
use ExpressionBuilder\Expression\Type\TString;

/**
 * Class Field
 *
 * @package ExpressionBuilder\Expression
 */
class Field implements TBoolean, TNumeric, TString, TDateTime
{
    /**
     * @var string
     */
    private string $name;

    /**
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param Dispatcher $dispatcher
     *
     * @return mixed
     * @throws ExpressionBuilderError
     */
    public function resolve(Dispatcher $dispatcher): mixed
    {
        return $dispatcher->dispatch($this);
    }
}
