<?php

declare(strict_types=1);

namespace ExpressionBuilder\Expression;

use ExpressionBuilder\Dispatcher;
use ExpressionBuilder\Expression;
use ExpressionBuilder\Expression\Type\TBoolean;
use ExpressionBuilder\Expression\Type\TDateTime;
use ExpressionBuilder\Expression\Type\TNumeric;
use ExpressionBuilder\Expression\Type\TString;

/**
 * Class Literal
 *
 * @package ExpressionBuilder\Expression
 */
class Literal implements TBoolean, TNumeric, TString, TDateTime
{
    /** @var mixed */
    private mixed $value;

    /**
     * @param mixed $value
     */
    public function __construct(mixed $value)
    {
        if (is_array($value)) {
            $arr = [];
            foreach ($value as $item) {
                if (!($item instanceof Expression)) {
                    $arr[] = new Literal($item);
                } else {
                    $arr[] = $item;
                }
            }
            $value = $arr;
        }
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    public function resolve(Dispatcher $dispatcher): mixed
    {
        return $dispatcher->dispatch($this);
    }
}
