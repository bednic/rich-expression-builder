<?php

declare(strict_types=1);

namespace ExpressionBuilder\Expression;

use ExpressionBuilder\Dispatcher;
use ExpressionBuilder\Exception\ExpressionBuilderError;
use ExpressionBuilder\Expression;
use ExpressionBuilder\Expression\Type\TBoolean;
use ExpressionBuilder\Expression\Type\TDateTime;
use ExpressionBuilder\Expression\Type\TNumeric;
use ExpressionBuilder\Expression\Type\TString;
use JetBrains\PhpStorm\ArrayShape;

/**
 * Class Method
 *
 * @package ExpressionBuilder\Expression
 */
class Method implements Functional, TBoolean, TNumeric, TString, TDateTime
{
    /**
     * @var Operator
     */
    protected Operator $fn;
    /**
     * @var Expression[]
     */
    protected array $args;

    /**
     * Func constructor.
     *
     * @param Operator $func
     * @param array  $args
     */
    public function __construct(Operator $func, array $args)
    {
        $this->fn   = $func;
        $this->args = $args;
    }

    /**
     * @return Operator
     */
    public function getFn(): Operator
    {
        return $this->fn;
    }

    /**
     * @return Expression[]
     */
    public function getArgs(): array
    {
        return $this->args;
    }

    /**
     * @param Dispatcher $dispatcher
     *
     * @return mixed
     * @throws ExpressionBuilderError
     */
    public function resolve(Dispatcher $dispatcher): mixed
    {
        return $dispatcher->dispatch($this);
    }
}
