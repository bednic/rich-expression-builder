<?php

declare(strict_types=1);

namespace ExpressionBuilder\Expression;

use ExpressionBuilder\Dispatcher;
use ExpressionBuilder\Exception\ExpressionBuilderError;
use ExpressionBuilder\Expression;
use ExpressionBuilder\Expression\Type\TBoolean;

/**
 * Class Comparison
 *
 * @package ExpressionBuilder\Expression
 */
class Comparison implements Binary, TBoolean
{
    /**
     * @var Expression
     */
    private Expression $left;
    /**
     * @var Operator
     */
    private Operator $op;
    /**
     * @var Expression
     */
    private Expression $right;

    /**
     * @param Expression $left
     * @param Operator   $operator
     * @param Expression $right
     */
    public function __construct(Expression $left, Operator $operator, Expression $right)
    {
        $this->left  = $left;
        $this->op    = $operator;
        $this->right = $right;
    }

    /**
     * @return Expression
     */
    public function getLeft(): Expression
    {
        return $this->left;
    }

    /**
     * @return Expression
     */
    public function getRight(): Expression
    {
        return $this->right;
    }

    /**
     * @return Operator
     */
    public function getOp(): Operator
    {
        return $this->op;
    }

    /**
     * @param Dispatcher $dispatcher
     *
     * @return mixed
     * @throws ExpressionBuilderError
     */
    public function resolve(Dispatcher $dispatcher): mixed
    {
        return $dispatcher->dispatch($this);
    }
}
