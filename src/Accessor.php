<?php

declare(strict_types=1);

namespace ExpressionBuilder;

use ExpressionBuilder\Accessor\ObjectPropertyAccessor;
use ExpressionBuilder\Accessor\SimpleAccessor;
use ExpressionBuilder\Exception\ExpressionBuilderError;
use ExpressionBuilder\Expression\Field;

/**
 * Interface Accessor
 * Use this for accessing to Field, parse field string by yourself.
 * Can serve for requiring joins for SQL, or traversing trough objects
 *
 * @see ObjectPropertyAccessor
 * @see SimpleAccessor
 *
 * @package ExpressionBuilder
 */
interface Accessor
{
    /**
     * The __invoke method is called when a script tries to call an object as a function.
     *
     * @param Field $field
     * @param array $args
     *
     * @return mixed
     * @throws ExpressionBuilderError
     * @link https://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.invoke
     */
    public function __invoke(Field $field, array $args = []): mixed;
}
