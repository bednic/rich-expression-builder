<?php

declare(strict_types=1);

namespace ExpressionBuilder;

use ExpressionBuilder\Expression\Comparison;
use ExpressionBuilder\Expression\Field;
use ExpressionBuilder\Expression\Literal;
use ExpressionBuilder\Expression\Math;
use ExpressionBuilder\Expression\Method;
use ExpressionBuilder\Expression\Operator;
use ExpressionBuilder\Expression\Type\TBoolean;
use ExpressionBuilder\Expression\Type\TDateTime;
use ExpressionBuilder\Expression\Type\TNumeric;
use ExpressionBuilder\Expression\Type\TString;

/**
 * Class Ex - shortcut to build expressions and create expression tree
 *
 * @package ExpressionBuilder
 */
final class Ex
{
    /**
     * @param Expression $left
     * @param Expression $right
     *
     * @return TBoolean
     */
    public static function eq(Expression $left, Expression $right): TBoolean
    {
        return new Comparison($left, Operator::LOGICAL_EQUAL, $right);
    }

    /**
     * @param Expression $left
     * @param Expression $right
     *
     * @return TBoolean
     */
    public static function ne(Expression $left, Expression $right): TBoolean
    {
        return new Comparison($left, Operator::LOGICAL_NOT_EQUAL, $right);
    }

    /**
     * @param Expression $left
     * @param Expression $right
     *
     * @return TBoolean
     */
    public static function lt(Expression $left, Expression $right): TBoolean
    {
        return new Comparison($left, Operator::LOGICAL_LOWER_THAN, $right);
    }

    /**
     * @param Expression $left
     * @param Expression $right
     *
     * @return TBoolean
     */
    public static function le(Expression $left, Expression $right): TBoolean
    {
        return new Comparison($left, Operator::LOGICAL_LOWER_THAN_OR_EQUAL, $right);
    }

    /**
     * @param Expression $left
     * @param Expression $right
     *
     * @return TBoolean
     */
    public static function gt(Expression $left, Expression $right): TBoolean
    {
        return new Comparison($left, Operator::LOGICAL_GREATER_THAN, $right);
    }

    /**
     * @param Expression $left
     * @param Expression $right
     *
     * @return TBoolean
     */
    public static function ge(Expression $left, Expression $right): TBoolean
    {
        return new Comparison($left, Operator::LOGICAL_GREATER_THAN_OR_EQUAL, $right);
    }

    /**
     * @param Field                                                                        $left
     * @param TNumeric[]|TDateTime[]|TString[]|int[]|float[]|string[]|\DateTimeInterface[] $right
     *
     * @return TBoolean
     */
    public static function in(Field $left, array $right): TBoolean
    {
        return new Comparison($left, Operator::LOGICAL_IN, new Literal($right));
    }

    /**
     * @param Expression         $left
     * @param TNumeric|TDateTime $from
     * @param TNumeric|TDateTime $to
     *
     * @return TBoolean
     */
    public static function be(Expression $left, TNumeric|TDateTime $from, TNumeric|TDateTime $to): TBoolean
    {
        return new Comparison($left, Operator::LOGICAL_BETWEEN, new Literal([$from, $to]));
    }

    /**
     * @param TBoolean $left
     * @param TBoolean $right
     *
     * @return TBoolean
     */
    public static function and(TBoolean $left, TBoolean $right): TBoolean
    {
        return new Comparison($left, Operator::LOGICAL_AND, $right);
    }

    /**
     * @param TBoolean $left
     * @param TBoolean $right
     *
     * @return TBoolean
     */
    public static function or(TBoolean $left, TBoolean $right): TBoolean
    {
        return new Comparison($left, Operator::LOGICAL_OR, $right);
    }

    /**
     * @param TBoolean $expression
     *
     * @return TBoolean
     */
    public static function not(TBoolean $expression): TBoolean
    {
        return new Method(Operator::FUNCTION_NOT, [$expression]);
    }

    /**
     * @param TString $subject
     *
     * @return TNumeric
     */
    public static function length(TString $subject): TNumeric
    {
        return new Method(Operator::FUNCTION_LENGTH, [$subject]);
    }

    /**
     * @param TString $subject
     * @param TString $append
     *
     * @return TString
     */
    public static function concat(TString $subject, TString $append): TString
    {
        return new Method(Operator::FUNCTION_CONCAT, [$subject, $append]);
    }

    /**
     * @param TString $haystack
     * @param TString $needle
     *
     * @return TBoolean
     */
    public static function contains(TString $haystack, TString $needle): TBoolean
    {
        return new Method(Operator::FUNCTION_CONTAINS, [$haystack, $needle]);
    }

    /**
     * @param TString $haystack
     * @param TString $needle
     *
     * @return TBoolean
     */
    public static function startsWith(TString $haystack, TString $needle): TBoolean
    {
        return new Method(Operator::FUNCTION_STARTS_WITH, [$haystack, $needle]);
    }

    /**
     * @param TString $haystack
     * @param TString $needle
     *
     * @return TBoolean
     */
    public static function endsWith(TString $haystack, TString $needle): TBoolean
    {
        return new Method(Operator::FUNCTION_ENDS_WITH, [$haystack, $needle]);
    }

    /**
     * @param TString $haystack
     * @param TString $needle
     *
     * @return TNumeric
     */
    public static function indexOf(TString $haystack, TString $needle): TNumeric
    {
        return new Method(Operator::FUNCTION_INDEX_OF, [$haystack, $needle]);
    }

    /**
     * @param TString       $string
     * @param TNumeric      $start
     * @param TNumeric|null $length
     *
     * @return TString
     */
    public static function substring(TString $string, TNumeric $start, TNumeric $length = null): TString
    {
        if ($length) {
            return new Method(Operator::FUNCTION_SUBSTRING, [$string, $start, $length]);
        }
        return new Method(Operator::FUNCTION_SUBSTRING, [$string, $start]);
    }

    /**
     * @param TString $subject
     * @param TString $pattern
     *
     * @return TBoolean
     */
    public static function matchesPattern(TString $subject, TString $pattern): TBoolean
    {
        return new Method(Operator::FUNCTION_MATCHES_PATTERN, [$pattern, $subject]);
    }

    /**
     * @param TString $subject
     *
     * @return TString
     */
    public static function toLower(TString $subject): TString
    {
        return new Method(Operator::FUNCTION_TO_LOWER, [$subject]);
    }

    /**
     * @param TString $subject
     *
     * @return TString
     */
    public static function toUpper(TString $subject): TString
    {
        return new Method(Operator::FUNCTION_TO_UPPER, [$subject]);
    }

    /**
     * @param TString $subject
     *
     * @return TString
     */
    public static function trim(TString $subject): TString
    {
        return new Method(Operator::FUNCTION_TRIM, [$subject]);
    }

    /**
     * @param TNumeric $x
     * @param TNumeric $y
     *
     * @return TNumeric
     */
    public static function add(TNumeric $x, TNumeric $y): TNumeric
    {
        return new Math($x, Operator::ARITHMETIC_ADDITION, $y);
    }

    /**
     * @param TNumeric $x
     * @param TNumeric $y
     *
     * @return TNumeric
     */
    public static function sub(TNumeric $x, TNumeric $y): TNumeric
    {
        return new Math($x, Operator::ARITHMETIC_SUBTRACTION, $y);
    }

    /**
     * @param TNumeric $x
     * @param TNumeric $y
     *
     * @return TNumeric
     */
    public static function mul(TNumeric $x, TNumeric $y): TNumeric
    {
        return new Math($x, Operator::ARITHMETIC_MULTIPLICATION, $y);
    }

    /**
     * @param TNumeric $x
     * @param TNumeric $y
     *
     * @return TNumeric
     */
    public static function div(TNumeric $x, TNumeric $y): TNumeric
    {
        return new Math($x, Operator::ARITHMETIC_DIVISION, $y);
    }

    /**
     * @param TNumeric $x
     * @param TNumeric $y
     *
     * @return TNumeric
     */
    public static function mod(TNumeric $x, TNumeric $y): TNumeric
    {
        return new Math($x, Operator::ARITHMETIC_MODULO, $y);
    }

    /**
     * @param TNumeric $value
     *
     * @return TNumeric
     */
    public static function ceiling(TNumeric $value): TNumeric
    {
        return new Method(Operator::FUNCTION_CEILING, [$value]);
    }

    /**
     * @param TNumeric $value
     *
     * @return TNumeric
     */
    public static function floor(TNumeric $value): TNumeric
    {
        return new Method(Operator::FUNCTION_FLOOR, [$value]);
    }

    /**
     * @param TNumeric $value
     *
     * @return TNumeric
     */
    public static function round(TNumeric $value): TNumeric
    {
        return new Method(Operator::FUNCTION_ROUND, [$value]);
    }

    /**
     * @param TDateTime $datetime
     *
     * @return TDateTime
     */
    public static function date(TDateTime $datetime): TDateTime
    {
        return new Method(Operator::FUNCTION_DATE, [$datetime]);
    }

    /**
     * @param TDateTime $datetime
     *
     * @return TNumeric
     */
    public static function day(TDateTime $datetime): TNumeric
    {
        return new Method(Operator::FUNCTION_DAY, [$datetime]);
    }

    /**
     * @param TDateTime $datetime
     *
     * @return TNumeric
     */
    public static function hour(TDateTime $datetime): TNumeric
    {
        return new Method(Operator::FUNCTION_HOUR, [$datetime]);
    }

    /**
     * @param TDateTime $datetime
     *
     * @return TNumeric
     */
    public static function minute(TDateTime $datetime): TNumeric
    {
        return new Method(Operator::FUNCTION_MINUTE, [$datetime]);
    }

    /**
     * @param TDateTime $datetime
     *
     * @return TNumeric
     */
    public static function month(TDateTime $datetime): TNumeric
    {
        return new Method(Operator::FUNCTION_MONTH, [$datetime]);
    }

    /**
     * @param TDateTime $datetime
     *
     * @return TNumeric
     */
    public static function second(TDateTime $datetime): TNumeric
    {
        return new Method(Operator::FUNCTION_SECOND, [$datetime]);
    }

    /**
     * @param TDateTime $datetime
     *
     * @return TDateTime
     */
    public static function time(TDateTime $datetime): TDateTime
    {
        return new Method(Operator::FUNCTION_TIME, [$datetime]);
    }

    /**
     * @param TDateTime $datetime
     *
     * @return TNumeric
     */
    public static function year(TDateTime $datetime): TNumeric
    {
        return new Method(Operator::FUNCTION_YEAR, [$datetime]);
    }

    /**
     * @param mixed $value
     *
     * @return Literal
     */
    public static function literal(mixed $value): Literal
    {
        return new Literal($value);
    }

    /**
     * @param string $name
     *
     * @return Field
     */
    public static function field(string $name): Field
    {
        return new Field($name);
    }
}
