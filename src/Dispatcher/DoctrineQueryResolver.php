<?php

declare(strict_types=1);

namespace ExpressionBuilder\Dispatcher;

use DateTimeInterface;
use Doctrine\ORM\Query\Expr;
use ExpressionBuilder\Accessor;
use ExpressionBuilder\Accessor\SimpleAccessor;
use ExpressionBuilder\Exception\MissingDependency;
use ExpressionBuilder\Exception\NotImplemented;
use ExpressionBuilder\Expression\Operator;

/**
 * Class DoctrineQueryExpressionBuilder
 *
 * @package JSONAPI\URI\Filtering\Builder
 */
class DoctrineQueryResolver extends Resolver
{
    /**
     * @var Expr
     */
    private Expr $exp;

    /**
     * @param Accessor|null $accessor
     */
    public function __construct(Accessor $accessor = null)
    {
        parent::__construct($accessor ?? new SimpleAccessor());
        if (!class_exists('Doctrine\ORM\Query\Expr')) {
            throw new MissingDependency(
                'For using ' . __CLASS__ . ' you need install [doctrine/orm] <i>composer require doctrine/orm</i>.'
            );
        }
        $this->exp = new Expr();
    }

    /**
     * @inheritDoc
     */
    public function resolveComparisonAnd(mixed $left, mixed $right): Expr\Andx
    {
        return $this->exp->andX($left, $right);
    }

    /**
     * @inheritDoc
     */
    public function resolveComparisonOr(mixed $left, mixed $right): Expr\Orx
    {
        return $this->exp->orX($left, $right);
    }

    /**
     * @inheritDoc
     */
    public function resolveComparisonEqual(mixed $left, mixed $right): Expr\Comparison
    {
        return $this->exp->eq($left, $right);
    }

    /**
     * @inheritDoc
     */
    public function resolveComparisonNotEqual(mixed $left, mixed $right): Expr\Comparison
    {
        return $this->exp->neq($left, $right);
    }

    /**
     * @inheritDoc
     */
    public function resolveComparisonGreaterThan(mixed $left, mixed $right): Expr\Comparison
    {
        return $this->exp->gt($left, $right);
    }

    /**
     * @inheritDoc
     */
    public function resolveComparisonGreaterThenOrEqual(mixed $left, mixed $right): Expr\Comparison
    {
        return $this->exp->gte($left, $right);
    }

    /**
     * @inheritDoc
     */
    public function resolveComparisonLowerThen(mixed $left, mixed $right): Expr\Comparison
    {
        return $this->exp->lt($left, $right);
    }

    /**
     * @inheritDoc
     */
    public function resolveComparisonLowerThenOrEqual(mixed $left, mixed $right): Expr\Comparison
    {
        return $this->exp->lte($left, $right);
    }

    /**
     * @inheritDoc
     */
    public function resolveComparisonIn(mixed $left, mixed $right): Expr\Func
    {
        return $this->exp->in($left, $right);
    }

    /**
     * @inheritDoc
     */
    public function resolveMathAddition(mixed $left, mixed $right): Expr\Math
    {
        return $this->exp->sum($left, $right);
    }

    /**
     * @inheritDoc
     */
    public function resolveMathSubtraction(mixed $left, mixed $right): Expr\Math
    {
        return $this->exp->diff($left, $right);
    }

    /**
     * @inheritDoc
     */
    public function resolveMathMultiplication(mixed $left, mixed $right): Expr\Math
    {
        return $this->exp->prod($left, $right);
    }

    /**
     * @inheritDoc
     */
    public function resolveMathDivision(mixed $left, mixed $right): Expr\Math
    {
        return $this->exp->quot($left, $right);
    }

    /**
     * @inheritDoc
     */
    public function resolveMathModulo(mixed $left, mixed $right): Expr\Math
    {
        throw new NotImplemented(Operator::ARITHMETIC_MODULO);
    }

    /**
     * @inheritDoc
     */
    public function resolveMethodNot(mixed $args): Expr\Func
    {
        return $this->exp->not($args);
    }

    /**
     * @inheritDoc
     */
    public function resolveMethodToUpper(mixed $args): Expr\Func
    {
        return $this->exp->upper($args);
    }

    /**
     * @inheritDoc
     */
    public function resolveMethodToLower(mixed $args): Expr\Func
    {
        return $this->exp->lower($args);
    }

    /**
     * @inheritDoc
     */
    public function resolveMethodTrim(mixed $args): Expr\Func
    {
        return $this->exp->trim($args);
    }

    /**
     * @inheritDoc
     */
    public function resolveMethodLength(mixed $args): Expr\Func
    {
        return $this->exp->length($args);
    }

    /**
     * @inheritDoc
     */
    public function resolveMethodConcat(mixed $args): Expr\Func
    {
        return $this->exp->concat(...$args);
    }

    /**
     * @inheritDoc
     */
    public function resolveMethodContains(mixed $args): Expr\Comparison
    {
        $value = trim((string)$args[1], '\'');
        return $this->exp->like($args[0], "'%{$value}%'");
    }

    /**
     * @inheritDoc
     */
    public function resolveMethodStartsWith(mixed $args): Expr\Comparison
    {
        $value = trim((string)$args[1], '\'');
        return $this->exp->like($args[0], "'{$value}%'");
    }

    /**
     * @inheritDoc
     */
    public function resolveMethodEndsWith(mixed $args): Expr\Comparison
    {
        $value = trim((string)$args[1], '\'');
        return $this->exp->like($args[0], "'%{$value}'");
    }

    /**
     * @inheritDoc
     */
    public function resolveMethodSubstring(array $args): Expr\Func
    {
        return $this->exp->substring(...$args);
    }

    /**
     * @inheritDoc
     */
    public function resolveMethodIndexOf(mixed $args): mixed
    {
        throw new NotImplemented(Operator::FUNCTION_INDEX_OF);
    }

    /**
     * @inheritDoc
     */
    public function resolveMethodMatchesPattern(mixed $args): mixed
    {
        throw new NotImplemented(Operator::FUNCTION_MATCHES_PATTERN);
    }

    /**
     * @inheritDoc
     */
    public function resolveMethodCeiling(mixed $args): mixed
    {
        throw new NotImplemented(Operator::FUNCTION_CEILING);
    }

    /**
     * @inheritDoc
     */
    public function resolveMethodFloor(mixed $args): mixed
    {
        throw new NotImplemented(Operator::FUNCTION_FLOOR);
    }

    /**
     * @inheritDoc
     */
    public function resolveMethodRound(mixed $args): mixed
    {
        throw new NotImplemented(Operator::FUNCTION_ROUND);
    }

    /**
     * @inheritDoc
     */
    public function resolveLiteral(mixed $expression): Expr\Literal
    {
        if ($expression instanceof DateTimeInterface) {
            $expression = $expression->format(DATE_ATOM);
        }
        return $this->exp->literal($expression);
    }

    public function resolveMethodDate(mixed $args): mixed
    {
        throw new NotImplemented(Operator::FUNCTION_ROUND);
    }

    public function resolveMethodDay(mixed $args): mixed
    {
        throw new NotImplemented(Operator::FUNCTION_ROUND);
    }

    public function resolveMethodHour(mixed $args): mixed
    {
        throw new NotImplemented(Operator::FUNCTION_ROUND);
    }

    public function resolveMethodMinute(mixed $args): mixed
    {
        throw new NotImplemented(Operator::FUNCTION_ROUND);
    }

    public function resolveMethodMonth(mixed $args): mixed
    {
        throw new NotImplemented(Operator::FUNCTION_ROUND);
    }

    public function resolveMethodSecond(mixed $args): mixed
    {
        throw new NotImplemented(Operator::FUNCTION_ROUND);
    }

    public function resolveMethodTime(mixed $args): mixed
    {
        throw new NotImplemented(Operator::FUNCTION_ROUND);
    }

    public function resolveMethodYear(mixed $args): mixed
    {
        throw new NotImplemented(Operator::FUNCTION_ROUND);
    }

    public function resolveField(mixed $expression): mixed
    {
        return $this->getAccessor()($expression);
    }

    public function resolveComparisonBetween(mixed $left, mixed $right): string
    {
        return $this->exp->between($left, ...$right);
    }
}
