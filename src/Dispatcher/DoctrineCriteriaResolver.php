<?php

namespace ExpressionBuilder\Dispatcher;

use Doctrine\Common\Collections\Expr\Comparison;
use Doctrine\Common\Collections\Expr\CompositeExpression;
use Doctrine\Common\Collections\ExpressionBuilder as Expr;
use ExpressionBuilder\Accessor;
use ExpressionBuilder\Accessor\SimpleAccessor;
use ExpressionBuilder\Exception\MissingDependency;
use ExpressionBuilder\Exception\NotImplemented;
use ExpressionBuilder\Expression\Field;
use ExpressionBuilder\Expression\Literal;
use ExpressionBuilder\Expression\Operator;

/**
 * Class DoctrineCriteriaResolver
 *
 * @package ExpressionBuilder\Dispatcher
 */
class DoctrineCriteriaResolver extends Resolver
{
    private Expr $exp;

    /**
     * @param Accessor|null $accessor
     */
    public function __construct(Accessor $accessor = null)
    {
        parent::__construct($accessor ?? new SimpleAccessor());
        if (!class_exists('Doctrine\Common\Collections\ExpressionBuilder')) {
            throw new MissingDependency(
                'For using ' . __CLASS__ . ' you need install [doctrine/collection] ' .
                '<i>composer require doctrine/collection</i>.'
            );
        }
        $this->exp = new Expr();
    }

    /**
     * @inheritDoc
     */
    protected function resolveField(Field $expression): mixed
    {
        return $this->getAccessor()($expression);
    }

    /**
     * @inheritDoc
     */
    protected function resolveLiteral(Literal $expression): mixed
    {
        $value = $expression->getValue();
        if (is_array($value)) {
            $value = array_map(fn($item) => $item->resolve($this), $value);
        }
        return $value;
    }

    /**
     * @inheritDoc
     */
    protected function resolveComparisonEqual(mixed $left, mixed $right): Comparison
    {
        return $this->exp->eq($left, $right);
    }

    /**
     * @inheritDoc
     */
    protected function resolveComparisonNotEqual(mixed $left, mixed $right): Comparison
    {
        return $this->exp->neq($left, $right);
    }

    /**
     * @inheritDoc
     */
    protected function resolveComparisonLowerThen(mixed $left, mixed $right): Comparison
    {
        return $this->exp->lt($left, $right);
    }

    /**
     * @inheritDoc
     */
    protected function resolveComparisonLowerThenOrEqual(mixed $left, mixed $right): Comparison
    {
        return $this->exp->lte($left, $right);
    }

    /**
     * @inheritDoc
     */
    protected function resolveComparisonGreaterThan(mixed $left, mixed $right): Comparison
    {
        return $this->exp->gt($left, $right);
    }

    /**
     * @inheritDoc
     */
    protected function resolveComparisonGreaterThenOrEqual(mixed $left, mixed $right): Comparison
    {
        return $this->exp->gte($left, $right);
    }

    /**
     * @inheritDoc
     */
    protected function resolveComparisonIn(mixed $left, mixed $right): Comparison
    {
        return $this->exp->in($left, $right);
    }

    /**
     * @inheritDoc
     */
    protected function resolveComparisonOr(mixed $left, mixed $right): CompositeExpression
    {
        return $this->exp->orX($left, $right);
    }

    /**
     * @inheritDoc
     */
    protected function resolveComparisonAnd(mixed $left, mixed $right): CompositeExpression
    {
        return $this->exp->andX($left, $right);
    }

    /**
     * @inheritDoc
     */
    protected function resolveComparisonBetween(mixed $left, mixed $right): mixed
    {
        throw new NotImplemented(Operator::LOGICAL_BETWEEN);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodConcat(array $args): mixed
    {
        throw new NotImplemented(Operator::FUNCTION_CONCAT);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodLength(array $args): mixed
    {
        throw new NotImplemented(Operator::FUNCTION_LENGTH);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodContains(array $args): Comparison
    {
        return $this->exp->contains(...$args);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodEndsWith(array $args): Comparison
    {
        return $this->exp->endsWith(...$args);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodStartsWith(array $args): Comparison
    {
        return $this->exp->startsWith(...$args);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodIndexOf(array $args): mixed
    {
        throw new NotImplemented(Operator::FUNCTION_INDEX_OF);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodSubstring(array $args): mixed
    {
        throw new NotImplemented(Operator::FUNCTION_SUBSTRING);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodToLower(array $args): mixed
    {
        throw new NotImplemented(Operator::FUNCTION_TO_LOWER);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodToUpper(array $args): mixed
    {
        throw new NotImplemented(Operator::FUNCTION_TO_UPPER);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodTrim(array $args): mixed
    {
        throw new NotImplemented(Operator::FUNCTION_TRIM);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodMatchesPattern(array $args): mixed
    {
        throw new NotImplemented(Operator::FUNCTION_MATCHES_PATTERN);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodDate(array $args): mixed
    {
        throw new NotImplemented(Operator::FUNCTION_DATE);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodTime(array $args): mixed
    {
        throw new NotImplemented(Operator::FUNCTION_TIME);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodDay(array $args): mixed
    {
        throw new NotImplemented(Operator::FUNCTION_DAY);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodMonth(array $args): mixed
    {
        throw new NotImplemented(Operator::FUNCTION_MONTH);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodYear(array $args): mixed
    {
        throw new NotImplemented(Operator::FUNCTION_YEAR);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodHour(array $args): mixed
    {
        throw new NotImplemented(Operator::FUNCTION_HOUR);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodMinute(array $args): mixed
    {
        throw new NotImplemented(Operator::FUNCTION_MINUTE);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodSecond(array $args): mixed
    {
        throw new NotImplemented(Operator::FUNCTION_SECOND);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodFloor(array $args): mixed
    {
        throw new NotImplemented(Operator::FUNCTION_FLOOR);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodRound(array $args): mixed
    {
        throw new NotImplemented(Operator::FUNCTION_ROUND);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodCeiling(array $args): mixed
    {
        throw new NotImplemented(Operator::FUNCTION_CEILING);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodNot(array $args): mixed
    {
        throw new NotImplemented(Operator::FUNCTION_NOT);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMathAddition(mixed $left, mixed $right): mixed
    {
        throw new NotImplemented(Operator::ARITHMETIC_ADDITION);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMathSubtraction(mixed $left, mixed $right): mixed
    {
        throw new NotImplemented(Operator::ARITHMETIC_SUBTRACTION);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMathModulo(mixed $left, mixed $right): mixed
    {
        throw new NotImplemented(Operator::ARITHMETIC_MODULO);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMathDivision(mixed $left, mixed $right): mixed
    {
        throw new NotImplemented(Operator::ARITHMETIC_DIVISION);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMathMultiplication(mixed $left, mixed $right): mixed
    {
        throw new NotImplemented(Operator::ARITHMETIC_MULTIPLICATION);
    }
}
