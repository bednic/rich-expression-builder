<?php

/**
 * Created by tomas
 * at 14.02.2021 18:38
 */

declare(strict_types=1);

namespace ExpressionBuilder\Dispatcher;

use DateTimeInterface;
use ExpressionBuilder\Accessor;
use ExpressionBuilder\Accessor\SimpleAccessor;
use ExpressionBuilder\Exception\ExpressionBuilderError;
use ExpressionBuilder\Expression;
use ExpressionBuilder\Expression\Field;
use ExpressionBuilder\Expression\Literal;

/**
 * Class SQLResolver
 *
 * @package ExpressionBuilder\Dispatcher
 * @method string dispatch(Expression $expression)
 */
abstract class SQLResolver extends Resolver
{
    /**
     * @var array<string, mixed> params
     */
    protected array $params = [];

    public function __construct(Accessor $accessor = null)
    {
        parent::__construct($accessor ?? new SimpleAccessor());
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return array_values($this->params);
    }

    /**
     * @param string                $placeholder
     * @param float|bool|int|string $value
     *
     * @return void
     */
    protected function setValue(string $placeholder, float|bool|int|string $value): void
    {
        $this->params[$placeholder] = $value;
    }

    /**
     * Returns value if exist or null if key does not exist
     *
     * @param string $placeholder
     *
     * @return int|float|bool|string|null
     */
    protected function getValue(string $placeholder): float|bool|int|string|null
    {
        return $this->params[$placeholder] ?? null;
    }

    /**
     * @param Literal $expression
     *
     * @return string
     * @throws ExpressionBuilderError
     */
    protected function resolveLiteral(Literal $expression): string
    {
        $value = $expression->getValue();
        if (is_null($value)) {
            return 'NULL';
        }
        if ($value instanceof DateTimeInterface) {
            $value = $value->format(DATE_ATOM);
        }

        if (is_array($value)) {
            $args = [];
            foreach ($value as $item) {
                if (is_scalar($item)) {
                    $args[] = $this->addValue($item);
                } else {
                    $args[] = $item->resolve($this);
                }
            }
            return join(",", $args);
        }
        return $this->addValue($value);
    }

    /**
     * Add value to buffer and returns placeholder
     *
     * @param float|bool|int|string $value
     *
     * @return string
     */
    protected function addValue(float|bool|int|string $value): string
    {
        $index                      = count($this->params);
        $placeholder                = ':' . $index;
        $this->params[$placeholder] = $value;
        return $placeholder;
    }

    /**
     * @param Field $expression
     *
     * @return string
     * @throws ExpressionBuilderError
     */
    protected function resolveField(Field $expression): string
    {
        return $this->getAccessor()($expression);
    }

    /**
     * @inheritDoc
     */
    protected function resolveComparisonBetween(mixed $left, mixed $right): string
    {
        list($from, $to) = explode(',', $right);
        return '(' . $left . ' BETWEEN ' . $from . ' AND ' . $to . ')';
    }

    /**
     * @inheritDoc
     */
    protected function resolveComparisonEqual(mixed $left, mixed $right): string
    {
        if ($right === 'NULL') {
            return $left . ' IS NOT ' . $right;
        }
        if ($left === 'NULL') {
            return $right . ' IS NOT ' . $left;
        }
        return $left . ' = ' . $right;
    }

    /**
     * @inheritDoc
     */
    protected function resolveComparisonNotEqual(mixed $left, mixed $right): string
    {
        if ($right === 'NULL') {
            return $left . ' IS NOT ' . $right;
        }
        if ($left === 'NULL') {
            return $right . ' IS NOT ' . $left;
        }
        return $left . ' <> ' . $right;
    }

    /**
     * @inheritDoc
     */
    protected function resolveComparisonLowerThen(mixed $left, mixed $right): string
    {
        return $left . ' < ' . $right;
    }

    /**
     * @inheritDoc
     */
    protected function resolveComparisonLowerThenOrEqual(mixed $left, mixed $right): string
    {
        return $left . ' <= ' . $right;
    }

    /**
     * @inheritDoc
     */
    protected function resolveComparisonGreaterThan(mixed $left, mixed $right): string
    {
        return $left . ' > ' . $right;
    }

    /**
     * @inheritDoc
     */
    protected function resolveComparisonGreaterThenOrEqual(mixed $left, mixed $right): string
    {
        return $left . ' >= ' . $right;
    }

    /**
     * @inheritDoc
     */
    protected function resolveComparisonIn(mixed $left, mixed $right): string
    {
        return $left . ' IN (' . $right . ')';
    }

    /**
     * @inheritDoc
     */
    protected function resolveComparisonOr(mixed $left, mixed $right): string
    {
        return '(' . $left . ' OR ' . $right . ')';
    }

    /**
     * @inheritDoc
     */
    protected function resolveComparisonAnd(mixed $left, mixed $right): string
    {
        return '(' . $left . ' AND ' . $right . ')';
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodContains(array $args): string
    {
        list($field, $placeholder) = $args;
        $this->setValue($placeholder, '%' . $this->getValue($placeholder) . '%');
        return sprintf('%s LIKE %s', $field, $placeholder);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodEndsWith(array $args): string
    {
        list($field, $placeholder) = $args;
        $this->setValue($placeholder, '%' . $this->getValue($placeholder));
        return sprintf('%s LIKE %s', $field, $placeholder);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodStartsWith(array $args): string
    {
        list($field, $placeholder) = $args;
        $this->setValue($placeholder, $this->getValue($placeholder) . '%');
        return sprintf('%s LIKE %s', $field, $placeholder);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodToLower(array $args): string
    {
        return sprintf('LOWER(%s)', ...$args);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodToUpper(array $args): string
    {
        return sprintf('UPPER(%s)', ...$args);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodTrim(array $args): string
    {
        return sprintf('TRIM(%s)', ...$args);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodRound(array $args): string
    {
        return sprintf('ROUND(%s)', ...$args);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodNot(array $args): string
    {
        return sprintf('NOT (%s)', ...$args);
    }
}
