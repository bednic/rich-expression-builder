<?php

namespace ExpressionBuilder\Dispatcher\Domain;

use ExpressionBuilder\Exception\ExpressionBuilderError;
use ExpressionBuilder\Exception\UnknownComparisonOperator;
use ExpressionBuilder\Expression\Comparison;
use ExpressionBuilder\Expression\Operator;

trait Comparisons
{
    /**
     * @param Comparison $expression
     *
     * @return mixed
     * @throws ExpressionBuilderError
     * @throws UnknownComparisonOperator
     */
    private function resolveComparison(Comparison $expression): mixed
    {
        $left  = $expression->getLeft()->resolve($this);
        $right = $expression->getRight()->resolve($this);
        return match ($expression->getOp()) {
            Operator::LOGICAL_EQUAL                 => $this->resolveComparisonEqual($left, $right),
            Operator::LOGICAL_NOT_EQUAL             => $this->resolveComparisonNotEqual($left, $right),
            Operator::LOGICAL_LOWER_THAN            => $this->resolveComparisonLowerThen($left, $right),
            Operator::LOGICAL_LOWER_THAN_OR_EQUAL   => $this->resolveComparisonLowerThenOrEqual($left, $right),
            Operator::LOGICAL_GREATER_THAN          => $this->resolveComparisonGreaterThan($left, $right),
            Operator::LOGICAL_GREATER_THAN_OR_EQUAL => $this->resolveComparisonGreaterThenOrEqual($left, $right),
            Operator::LOGICAL_IN                    => $this->resolveComparisonIn($left, $right),
            Operator::LOGICAL_OR                    => $this->resolveComparisonOr($left, $right),
            Operator::LOGICAL_AND                   => $this->resolveComparisonAnd($left, $right),
            Operator::LOGICAL_BETWEEN               => $this->resolveComparisonBetween($left, $right),
            default                                 => throw new UnknownComparisonOperator($expression->getOp())
        };
    }
    /**
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     * @throws UnknownComparisonOperator
     */
    abstract protected function resolveComparisonEqual(mixed $left, mixed $right): mixed;

    /**
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     * @throws UnknownComparisonOperator
     */
    abstract protected function resolveComparisonNotEqual(mixed $left, mixed $right): mixed;

    /**
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     * @throws UnknownComparisonOperator
     */
    abstract protected function resolveComparisonLowerThen(mixed $left, mixed $right): mixed;

    /**
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     * @throws UnknownComparisonOperator
     */
    abstract protected function resolveComparisonLowerThenOrEqual(mixed $left, mixed $right): mixed;

    /**
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     * @throws UnknownComparisonOperator
     */
    abstract protected function resolveComparisonGreaterThan(mixed $left, mixed $right): mixed;

    /**
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     * @throws UnknownComparisonOperator
     */
    abstract protected function resolveComparisonGreaterThenOrEqual(mixed $left, mixed $right): mixed;

    /**
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     * @throws UnknownComparisonOperator
     */
    abstract protected function resolveComparisonIn(mixed $left, mixed $right): mixed;

    /**
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     * @throws UnknownComparisonOperator
     */
    abstract protected function resolveComparisonOr(mixed $left, mixed $right): mixed;

    /**
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     * @throws UnknownComparisonOperator
     */
    abstract protected function resolveComparisonAnd(mixed $left, mixed $right): mixed;

    /**
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     * @throws ExpressionBuilderError
     */
    abstract protected function resolveComparisonBetween(mixed $left, mixed $right): mixed;
}
