<?php

namespace ExpressionBuilder\Dispatcher\Domain;

use ExpressionBuilder\Exception\ExpressionBuilderError;
use ExpressionBuilder\Exception\UnknownArithmeticOperation;
use ExpressionBuilder\Expression\Math;
use ExpressionBuilder\Expression\Operator;

trait Arithmetics
{
    /**
     * @param Math $expression
     *
     * @return mixed
     * @throws ExpressionBuilderError
     */
    private function resolveMath(Math $expression): mixed
    {
        $left  = $expression->getLeft()->resolve($this);
        $right = $expression->getRight()->resolve($this);
        return match ($expression->getOp()) {
            Operator::ARITHMETIC_ADDITION       => $this->resolveMathAddition($left, $right),
            Operator::ARITHMETIC_SUBTRACTION    => $this->resolveMathSubtraction($left, $right),
            Operator::ARITHMETIC_MODULO         => $this->resolveMathModulo($left, $right),
            Operator::ARITHMETIC_DIVISION       => $this->resolveMathDivision($left, $right),
            Operator::ARITHMETIC_MULTIPLICATION => $this->resolveMathMultiplication($left, $right),
            default                             => throw new UnknownArithmeticOperation($expression->getOp())
        };
    }

    /**
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     * @throws ExpressionBuilderError
     */
    abstract protected function resolveMathAddition(mixed $left, mixed $right): mixed;

    /**
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     * @throws ExpressionBuilderError
     */
    abstract protected function resolveMathSubtraction(mixed $left, mixed $right): mixed;

    /**
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     * @throws ExpressionBuilderError
     */
    abstract protected function resolveMathModulo(mixed $left, mixed $right): mixed;

    /**
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     * @throws ExpressionBuilderError
     */
    abstract protected function resolveMathDivision(mixed $left, mixed $right): mixed;

    /**
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     * @throws ExpressionBuilderError
     */
    abstract protected function resolveMathMultiplication(mixed $left, mixed $right): mixed;
}
