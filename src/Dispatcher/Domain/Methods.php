<?php

namespace ExpressionBuilder\Dispatcher\Domain;

use ExpressionBuilder\Exception\ExpressionBuilderError;
use ExpressionBuilder\Exception\UnknownFunction;
use ExpressionBuilder\Expression\Field;
use ExpressionBuilder\Expression\Literal;
use ExpressionBuilder\Expression\Method;
use ExpressionBuilder\Expression\Operator;

trait Methods
{
    /**
     * @param Method $expression
     *
     * @return mixed
     * @throws ExpressionBuilderError
     */
    private function resolveMethod(Method $expression): mixed
    {
        $args = array_map(fn($ex) => $ex->resolve($this), $expression->getArgs());
        return match ($expression->getFn()) {
            Operator::FUNCTION_CONCAT          => $this->resolveMethodConcat($args),
            Operator::FUNCTION_LENGTH          => $this->resolveMethodLength($args),
            Operator::FUNCTION_CONTAINS        => $this->resolveMethodContains($args),
            Operator::FUNCTION_ENDS_WITH       => $this->resolveMethodEndsWith($args),
            Operator::FUNCTION_STARTS_WITH     => $this->resolveMethodStartsWith($args),
            Operator::FUNCTION_INDEX_OF        => $this->resolveMethodIndexOf($args),
            Operator::FUNCTION_SUBSTRING       => $this->resolveMethodSubstring($args),
            Operator::FUNCTION_TO_LOWER        => $this->resolveMethodToLower($args),
            Operator::FUNCTION_TO_UPPER        => $this->resolveMethodToUpper($args),
            Operator::FUNCTION_TRIM            => $this->resolveMethodTrim($args),
            Operator::FUNCTION_MATCHES_PATTERN => $this->resolveMethodMatchesPattern($args),
            Operator::FUNCTION_DATE            => $this->resolveMethodDate($args),
            Operator::FUNCTION_TIME            => $this->resolveMethodTime($args),
            Operator::FUNCTION_DAY             => $this->resolveMethodDay($args),
            Operator::FUNCTION_MONTH           => $this->resolveMethodMonth($args),
            Operator::FUNCTION_YEAR            => $this->resolveMethodYear($args),
            Operator::FUNCTION_HOUR            => $this->resolveMethodHour($args),
            Operator::FUNCTION_MINUTE          => $this->resolveMethodMinute($args),
            Operator::FUNCTION_SECOND          => $this->resolveMethodSecond($args),
            Operator::FUNCTION_FLOOR           => $this->resolveMethodFloor($args),
            Operator::FUNCTION_ROUND           => $this->resolveMethodRound($args),
            Operator::FUNCTION_CEILING         => $this->resolveMethodCeiling($args),
            Operator::FUNCTION_NOT             => $this->resolveMethodNot($args),
            default                            => throw new UnknownFunction($expression->getFn())
        };
    }
    /**
     * @param Field $expression
     *
     * @return mixed
     * @throws ExpressionBuilderError
     */
    abstract protected function resolveField(Field $expression): mixed;

    /**
     * @param Literal $expression
     *
     * @return mixed
     * @throws ExpressionBuilderError
     */
    abstract protected function resolveLiteral(Literal $expression): mixed;

    /**
     * @param array $args
     *
     * @return mixed
     * @throws ExpressionBuilderError
     */
    abstract protected function resolveMethodConcat(array $args): mixed;

    /**
     * @param array $args
     *
     * @return mixed
     * @throws ExpressionBuilderError
     */
    abstract protected function resolveMethodLength(array $args): mixed;

    /***
     * @param array $args
     *
     * @return mixed
     * @throws ExpressionBuilderError
     */
    abstract protected function resolveMethodContains(array $args): mixed;

    /**
     * @param array $args
     *
     * @return mixed
     * @throws ExpressionBuilderError
     */
    abstract protected function resolveMethodEndsWith(array $args): mixed;

    /**
     * @param array $args
     *
     * @return mixed
     * @throws ExpressionBuilderError
     */
    abstract protected function resolveMethodStartsWith(array $args): mixed;

    /**
     * @param array $args
     *
     * @return mixed
     * @throws ExpressionBuilderError
     */
    abstract protected function resolveMethodIndexOf(array $args): mixed;

    /**
     * @param array $args
     *
     * @return mixed
     * @throws ExpressionBuilderError
     */
    abstract protected function resolveMethodSubstring(array $args): mixed;

    /**
     * @param array $args
     *
     * @return mixed
     * @throws ExpressionBuilderError
     */
    abstract protected function resolveMethodToLower(array $args): mixed;

    /**
     * @param array $args
     *
     * @return mixed
     * @throws ExpressionBuilderError
     */
    abstract protected function resolveMethodToUpper(array $args): mixed;

    /**
     * @param array $args
     *
     * @return mixed
     * @throws ExpressionBuilderError
     */
    abstract protected function resolveMethodTrim(array $args): mixed;

    /**
     * @param array $args
     *
     * @return mixed
     * @throws ExpressionBuilderError
     */
    abstract protected function resolveMethodMatchesPattern(array $args): mixed;

    /**
     * @param array $args
     *
     * @return mixed
     * @throws ExpressionBuilderError
     */
    abstract protected function resolveMethodDate(array $args): mixed;

    /**
     * @param array $args
     *
     * @return mixed
     * @throws ExpressionBuilderError
     */
    abstract protected function resolveMethodTime(array $args): mixed;

    /**
     * @param array $args
     *
     * @return mixed
     * @throws ExpressionBuilderError
     */
    abstract protected function resolveMethodDay(array $args): mixed;

    /**
     * @param array $args
     *
     * @return mixed
     * @throws ExpressionBuilderError
     */
    abstract protected function resolveMethodMonth(array $args): mixed;

    /**
     * @param array $args
     *
     * @return mixed
     * @throws ExpressionBuilderError
     */
    abstract protected function resolveMethodYear(array $args): mixed;

    /**
     * @param array $args
     *
     * @return mixed
     * @throws ExpressionBuilderError
     */
    abstract protected function resolveMethodHour(array $args): mixed;

    /**
     * @param array $args
     *
     * @return mixed
     * @throws ExpressionBuilderError
     */
    abstract protected function resolveMethodMinute(array $args): mixed;

    /**
     * @param array $args
     *
     * @return mixed
     * @throws ExpressionBuilderError
     */
    abstract protected function resolveMethodSecond(array $args): mixed;

    /**
     * @param array $args
     *
     * @return mixed
     * @throws ExpressionBuilderError
     */
    abstract protected function resolveMethodFloor(array $args): mixed;

    /**
     * @param array $args
     *
     * @return mixed
     * @throws ExpressionBuilderError
     */
    abstract protected function resolveMethodRound(array $args): mixed;

    /**
     * @param array $args
     *
     * @return mixed
     * @throws ExpressionBuilderError
     */
    abstract protected function resolveMethodCeiling(array $args): mixed;

    /**
     * @param array $args
     *
     * @return mixed
     * @throws ExpressionBuilderError
     */
    abstract protected function resolveMethodNot(array $args): mixed;
}
