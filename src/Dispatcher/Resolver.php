<?php

declare(strict_types=1);

namespace ExpressionBuilder\Dispatcher;

use ExpressionBuilder\Accessor;
use ExpressionBuilder\Dispatcher;
use ExpressionBuilder\Dispatcher\Domain\Arithmetics;
use ExpressionBuilder\Dispatcher\Domain\Comparisons;
use ExpressionBuilder\Dispatcher\Domain\Methods;
use ExpressionBuilder\Exception\ExpressionBuilderError;
use ExpressionBuilder\Exception\UnknownExpression;
use ExpressionBuilder\Expression;
use ExpressionBuilder\Expression\Comparison;
use ExpressionBuilder\Expression\Field;
use ExpressionBuilder\Expression\Literal;
use ExpressionBuilder\Expression\Math;
use ExpressionBuilder\Expression\Method;

/**
 * Class Resolver
 *
 * @package ExpressionBuilder\Dispatcher
 */
abstract class Resolver implements Dispatcher
{
    use Comparisons;
    use Methods;
    use Arithmetics;

    /**
     * @var Accessor
     */
    private Accessor $accessor;

    /**
     * ClosureResolver constructor.
     *
     * @param Accessor $accessor - accessor for object properties
     */
    public function __construct(Accessor $accessor)
    {
        $this->accessor = $accessor;
    }

    /**
     * @return Accessor
     */
    protected function getAccessor(): callable
    {
        return $this->accessor;
    }

    /**
     * @param Expression $expression
     *
     * @return mixed
     * @throws ExpressionBuilderError
     */
    public function dispatch(Expression $expression): mixed
    {
        return match (true) {
            $expression instanceof Comparison => $this->resolveComparison($expression),
            $expression instanceof Method     => $this->resolveMethod($expression),
            $expression instanceof Math       => $this->resolveMath($expression),
            $expression instanceof Field      => $this->resolveField($expression),
            $expression instanceof Literal    => $this->resolveLiteral($expression),
            default                           => throw new UnknownExpression(get_class($expression))
        };
    }
}
