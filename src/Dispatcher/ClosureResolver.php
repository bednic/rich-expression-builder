<?php

declare(strict_types=1);

namespace ExpressionBuilder\Dispatcher;

use Closure;
use DateTime;
use ExpressionBuilder\Accessor;
use ExpressionBuilder\Accessor\ObjectPropertyAccessor;
use ExpressionBuilder\Exception\ExpressionBuilderError;
use ExpressionBuilder\Exception\NotImplemented;
use ExpressionBuilder\Expression;
use ExpressionBuilder\Expression\Field;
use ExpressionBuilder\Expression\Literal;

/**
 * Class ClosureResolver
 *
 * @package ExpressionBuilder\Dispatcher
 * @method Closure dispatch(Expression $expression)
 */
class ClosureResolver extends Resolver
{
    /**
     * ClosureResolver constructor.
     *
     * @param Accessor|null $accessor - accessor for object properties
     */
    public function __construct(Accessor $accessor = null)
    {
        parent::__construct($accessor ?? new ObjectPropertyAccessor());
    }

    /**
     * @param Field $expression
     *
     * @return Closure
     */
    protected function resolveField(Field $expression): Closure
    {
        $callable = $this->getAccessor();
        return static fn($object) => $callable($expression, [$object]);
    }

    /**
     * @param Literal $expression
     *
     * @return Closure
     * @throws ExpressionBuilderError
     */
    protected function resolveLiteral(Literal $expression): Closure
    {
        $value = $expression->getValue();
        if (is_array($value)) {
            $value = array_map(fn($item) => /** @var Expression $item */ $item->resolve($this), $value);
        }
        return static fn($object) => is_array($value) ? array_map(fn($ex) => $ex($object), $value) : $value;
    }

    /**
     * @inheritDoc
     */
    protected function resolveComparisonEqual(mixed $left, mixed $right): Closure
    {
        return static fn($object) => $left($object) == $right($object);
    }

    /**
     * @inheritDoc
     */
    protected function resolveComparisonNotEqual(mixed $left, mixed $right): Closure
    {
        return static fn($object) => $left($object) != $right($object);
    }

    /**
     * @inheritDoc
     */
    protected function resolveComparisonLowerThen(mixed $left, mixed $right): Closure
    {
        return static fn($object) => $left($object) < $right($object);
    }

    /**
     * @inheritDoc
     */
    protected function resolveComparisonLowerThenOrEqual(mixed $left, mixed $right): Closure
    {
        return static fn($object) => $left($object) <= $right($object);
    }

    /**
     * @inheritDoc
     */
    protected function resolveComparisonGreaterThan(mixed $left, mixed $right): Closure
    {
        return static fn($object) => $left($object) > $right($object);
    }

    /**
     * @inheritDoc
     */
    protected function resolveComparisonGreaterThenOrEqual(mixed $left, mixed $right): Closure
    {
        return static fn($object) => $left($object) >= $right($object);
    }

    /**
     * @inheritDoc
     */
    protected function resolveComparisonIn(mixed $left, mixed $right): Closure
    {
        return static fn($object) => in_array($left($object), $right($object));
    }

    /**
     * @inheritDoc
     */
    protected function resolveComparisonOr(mixed $left, mixed $right): Closure
    {
        return static fn($object) => ($left($object) || $right($object));
    }

    /**
     * @inheritDoc
     */
    protected function resolveComparisonAnd(mixed $left, mixed $right): Closure
    {
        return static fn($object) => ($left($object) && $right($object));
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodConcat(array $args): Closure
    {
        return static fn($object) => join("", array_map(fn($closure) => $closure($object), $args));
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodLength(array $args): Closure
    {
        return static fn($object) => strlen(...array_map(fn($closure) => $closure($object), $args));
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodContains(array $args): Closure
    {
        return static fn($object) => strpos(...array_map(fn($closure) => $closure($object), $args)) !== false;
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodEndsWith(array $args): Closure
    {
        return static function ($object) use ($args) {
            $args = array_map(fn($closure) => $closure($object), $args);
            return str_ends_with($args[0], $args[1]);
        };
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodStartsWith(array $args): Closure
    {
        return static fn($object) => strpos(...array_map(fn($closure) => $closure($object), $args)) === 0;
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodIndexOf(array $args): Closure
    {
        return static fn($object) => strpos(...array_map(fn($closure) => $closure($object), $args));
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodSubstring(array $args): Closure
    {
        return static fn($object) => substr(...array_map(fn($closure) => $closure($object), $args));
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodToLower(array $args): Closure
    {
        return static fn($object) => strtolower(...array_map(fn($closure) => $closure($object), $args));
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodToUpper(array $args): Closure
    {
        return static fn($object) => strtoupper(...array_map(fn($closure) => $closure($object), $args));
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodTrim(array $args): Closure
    {
        return static fn($object) => trim(...array_map(fn($closure) => $closure($object), $args));
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodMatchesPattern(array $args): Closure
    {
        return static fn($object) => (bool)preg_match(...array_map(fn($closure) => $closure($object), $args));
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodDate(array $args): Closure
    {
        return static fn($object) => (new DateTime(array_map(fn($closure) => $closure($object), $args)[0]))
            ->format('Y-m-d');
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodTime(array $args): Closure
    {
        return static fn($object) => (new DateTime(array_map(fn($closure) => $closure($object), $args)[0]))
            ->format('H:i:s');
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodDay(array $args): Closure
    {
        return static fn($object) => (new DateTime(array_map(fn($closure) => $closure($object), $args)[0]))
            ->format('d');
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodMonth(array $args): Closure
    {
        return static fn($object) => (new DateTime(array_map(fn($closure) => $closure($object), $args)[0]))
            ->format('m');
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodYear(array $args): Closure
    {
        return static fn($object) => (new DateTime(array_map(fn($closure) => $closure($object), $args)[0]))
            ->format('Y');
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodHour(array $args): Closure
    {
        return static fn($object) => (new DateTime(array_map(fn($closure) => $closure($object), $args)[0]))
            ->format('H');
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodMinute(array $args): Closure
    {
        return static fn($object) => (new DateTime(array_map(fn($closure) => $closure($object), $args)[0]))
            ->format('i');
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodSecond(array $args): Closure
    {
        return static fn($object) => (new DateTime(array_map(fn($closure) => $closure($object), $args)[0]))
            ->format('s');
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodFloor(array $args): Closure
    {
        return static fn($object) => floor(...array_map(fn($closure) => $closure($object), $args));
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodRound(array $args): Closure
    {
        return static fn($object) => round(...array_map(fn($closure) => $closure($object), $args));
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodCeiling(array $args): Closure
    {
        return static fn($object) => ceil(...array_map(fn($closure) => $closure($object), $args));
    }

    /**
     * @inheritDoc
     */
    protected function resolveMethodNot(array $args): Closure
    {
        return static fn($object) => !((array_map(fn($closure) => $closure($object), $args))[0]);
    }

    /**
     * @inheritDoc
     */
    protected function resolveMathAddition(mixed $left, mixed $right): Closure
    {
        return static fn($object) => ($left($object) + $right($object));
    }

    /**
     * @inheritDoc
     */
    protected function resolveMathSubtraction(mixed $left, mixed $right): Closure
    {
        return static fn($object) => ($left($object) - $right($object));
    }

    /**
     * @inheritDoc
     */
    protected function resolveMathModulo(mixed $left, mixed $right): Closure
    {
        return static fn($object) => ($left($object) % $right($object));
    }

    /**
     * @inheritDoc
     */
    protected function resolveMathDivision(mixed $left, mixed $right): Closure
    {
        return static fn($object) => ($left($object) / $right($object));
    }

    /**
     * @inheritDoc
     */
    protected function resolveMathMultiplication(mixed $left, mixed $right): Closure
    {
        return static fn($object) => ($left($object) * $right($object));
    }

    /**
     * @inheritDoc
     */
    protected function resolveComparisonBetween(mixed $left, mixed $right): Closure
    {
        throw new NotImplemented(Expression\Operator::LOGICAL_BETWEEN);
    }
}
