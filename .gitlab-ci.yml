image: registry.gitlab.com/bednic/rich-expression-builder:8.1

services:
    - postgres:11-alpine

stages:
    - build
    - test
    - analyse
    - release

variables:
    SONAR_HOST_URL: "https://sonarcloud.io"
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"
    GIT_DEPTH: "0"
    POSTGRES_HOST: "postgres"
    POSTGRES_USER: "user"
    POSTGRES_PASSWORD: "secret"

build:
    stage: build
    script:
        - composer -q install
        - composer validate --no-check-all --strict
    artifacts:
        paths:
            - vendor

test:unit:
    stage: test
    script:
        - vendor/bin/phpunit
    artifacts:
        paths:
            - coverage.xml
            - logfile.xml
        expire_in: 1h

code_sniffer:
    stage: analyse
    script:
        - vendor/bin/phpcs

sonar:
    stage: analyse
    image:
        name: sonarsource/sonar-scanner-cli:latest
        entrypoint: [ "" ]
    cache:
        key: "${CI_JOB_NAME}"
        paths:
            - .sonar/cache
    script:
        - sonar-scanner
    allow_failure: true
    dependencies:
        - test:unit
    only:
        - '1.x'

phpstan:
    stage: analyse
    script:
        - vendor/bin/phpstan -n --no-ansi analyse

release:
    image: registry.gitlab.com/gitlab-org/release-cli
    stage: release
    rules:
        -   if: '$CI_COMMIT_TAG'
    script:
        - echo "Creating a release for version $CI_COMMIT_TAG"
    release:
        name: "$CI_COMMIT_TAG"
        description: "See [changelog](CHANGELOG.md)"
        tag_name: '$CI_COMMIT_TAG'
        ref: '$CI_COMMIT_TAG'

pages:
    stage: release
    rules:
        -   if: '$CI_COMMIT_TAG'
    image:
        name: phpdoc/phpdoc:3
        entrypoint: [ "" ]
    services: [ ]
    script:
        - phpdoc -d src -t public
    artifacts:
        paths:
            - public
        expire_in: 1h
