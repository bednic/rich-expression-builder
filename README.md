# Rich Expression Builder

Is a tool for creating expression tree to filter collection of objects. 
The main task is to be able filter object in array by their properties. 
Additionally, I will add SQLResolver to create WHERE filter for your query.
  
### Example

```php
<?php
$obj1        = new \stdClass();
$obj1->field = 1;
$obj2        = new \stdClass();
$obj2->field = 2;
$arr         = [$obj1, $obj2];
$expression  = Ex::and(
    Ex::eq(Ex::field('field'), Ex::literal(1)),
    Ex::gt(Ex::field('field'), Ex::literal(0))
);
$resolver    = new ClosureResolver();
$filter      = $resolver->dispatch($expression);
$result      = array_filter($arr, $filter);
```
> ### Result
```
array(1) {
  [0] =>
  class stdClass#410 (1) {
    public $field =>
    int(1)
  }
}
```
